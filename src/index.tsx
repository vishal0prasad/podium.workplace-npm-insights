//@ts-nocheck
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./index.css";
import i18n from "i18next";
import enUS from "./app/configs/translations/en-US.json";
import { ThemeProvider, constants, Snackbar } from "@udp-dataplatform/podiumuiengine";

// import OrdersLandingPage from "./app/pages/LandingPage/Pages/Orders/OrdersLandingPage";
import OrdersLanding from "./app/ordertracking/landing/OrdersLanding";
import Orders from "./app/ordertracking/orders";
import OrdersCalendarView from "./app/ordertracking/orders/components/ordersCalendarView";
import OrdersGanttView from "./app/ordertracking/orders/components/ordersGanttView";
import PublicPage from "./app/public/PublicPage"
import NotificationsLanding from "./app/ordertracking/notifications/NotificationsLanding";
import appRoutes from "./app/configs/data/appRoutes"

export const loadTranslation = (lang: string = constants.LANG_EN_US) => {
    switch (lang) {
        case constants.LANG_EN_US:
            i18n.addResourceBundle(lang, constants.LANG_TRANSLATION, enUS);
            break;
        default:
            i18n.addResourceBundle(lang, constants.LANG_TRANSLATION, enUS);
            break;
    }
};

export const OrdersLandingPageWrapperComponent = (props: any) => {
    return (
        <div>
            <OrdersLanding {...props} />
        </div>
    );
};


export const KeysToComponentMap = {
  OrdersLandingPageWrapperComponent
}

export const workflowRoutes = appRoutes.main
export const workflowPublicRoutes = appRoutes.publicMain

const isPublic= ()=> {
  return window.location.href.includes("/public/")
}



if (isPublic())
{
    const rootElement = document.getElementById("root");

     ReactDOM.render(
      <div>
    <BrowserRouter>
      <ThemeProvider>
      <Switch>
      {workflowPublicRoutes &&
            workflowPublicRoutes.map((app, index) => {
              return (
                <Route
                  exact={app.exact}
                  path={app.path}
                  key={index}
                  render={(props) =>
                    React.createElement(KeysToComponentMap[app.component], {...props}, null)
                  }
                />
              );
            })}
        </Switch>
        <Snackbar />
      </ThemeProvider>
    </BrowserRouter>;
    </div> ,
          rootElement
        );
}