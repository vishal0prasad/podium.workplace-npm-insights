
export const tableActionTemplate = (text:any, row:any,col:any, tableMetaData:any) => {
    tableMetaData.actionConfigs.cols.map((column: string) => {
        if (column === col.id) {
            text = `<ul class="action-buttons">
                        <li onclick="csEditFunction(event,${row.id})"><img src="${tableMetaData.actionConfigs.editIcon}" alt="" /></li>
                        <li onclick="csDeleteFunction(event,${row.id})"><img src="${tableMetaData.actionConfigs.deleteIcon}" alt="" /></li>
                    </ul>`
        }
    })
    return text;
}

