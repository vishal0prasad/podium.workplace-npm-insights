//exporting functions to mitigate scope issues
export const func = () => {}
export const editFunc = () => {}
export const deleteFunc = () => {}

export const templateFunction = (text:any, row:any,  col:any, tableMetaDataURLObject:any) => {
    if(tableMetaDataURLObject.table.menuAction.menuEnable && tableMetaDataURLObject.table.htmlEnable){
      tableMetaDataURLObject.table.menuAction.cols.map((column:string)=> {
        if(col.id === column){
          text = `<div class="dropdown"><button onclick='func("${row.id}")' class="dropbtn"><img src="${tableMetaDataURLObject.table.menuAction.menuIcon}"/></button><div id="${row.id}" class="dropdown-content">`
          tableMetaDataURLObject.table.menuAction.menuItems.map((menuItem:any) => {
            text = text + `<button className="dropButton" onclick='${menuItem.function}(event,"${row.id}")'>${menuItem.name}</button>`
          })
          text = text + `</div></div>`
        }
      })
    }
    if(tableMetaDataURLObject.table.tableActionsEnable  && tableMetaDataURLObject.table.htmlEnable){
      tableMetaDataURLObject.table.table_actions.map((table_action:any) => {
        if(table_action.action_enable){
          table_action.cols.map((column:string) => {
            if(col.id === column)
            text = `<select id="cars"><option value="volvo">Volvo</option><option value="saab">Saab</option></select>`
          })
        }
      })
    }
    return text
}
