import React from "react";
import { Avatar, Grid, Typography } from "@material-ui/core";
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import logo from '../../assets/InfraBuild.jpg';

// const AccordionSummary = withStyles({
//     root: {
//       marginBottom: -1,
//       minHeight: 56,
//       borderRadius: '8px 8px 0px 0px',
//       '&$expanded': {
//         minHeight: 56,
//       },
//     },
//     content: {
//       '&$expanded': {
//         margin: '12px 0',
//       },
//     },
//     expanded: {
//       backgroundColor: '#e4e5ea',
  
//     },
//   })(MuiAccordionSummary);

const useStyles = makeStyles((theme: Theme) => 

    createStyles({
        fontStylesHeading: {
            fontFamily: 'fontCirStdMedium',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'left',
            color: '#323232',
      
          },
          fontSecondary: {
            fontFamily: 'fontCirXxttReg',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'left',
            color: '#323232',
      
          }
    })
)

const CollapseTemplate = (props: any) => {

    const classes = useStyles();

    const { 
        crt_by, 
        ord_id,
        prev_values 
    } = props.data;

    let prev_values_length = 0;
    
    if (prev_values) {
        prev_values_length = (JSON.parse(JSON.parse(prev_values).json_agg).length) - 1;
    } 

    return (

        <Grid container spacing={2}>
            <Grid item>
                <Avatar alt="Infra Build" src={logo} />
            </Grid>
            <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                    <Grid item xs>
                        <Typography className={classes.fontStylesHeading}>
                            {crt_by} has {prev_values_length > 0 ? `made ${prev_values_length} update(s) to ${ord_id}` : `created Order with ID ${ord_id}`}
                        </Typography>
                        <Typography className={classes.fontSecondary}>
                            Escalator
                        </Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default CollapseTemplate;