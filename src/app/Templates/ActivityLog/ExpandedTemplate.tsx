//@ts-nocheck
import React from "react";
import { Divider, Grid, Typography } from "@material-ui/core";
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';


const useStyles = makeStyles((theme: Theme) =>

    createStyles({
        drawerColor: {
            backgroundColor: '#ffffff',
            borderRadius: '6px',
            marginTop: '0px',
        },
        fontSecRight: {
            fontFamily: 'fontCirXxttReg',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'right',
            color: '#62707d',
            wordBreak: 'break-word'
            // whiteSpace : 'nowrap',
        },
        fontSecRight1: {
            fontFamily: 'fontCirXxttReg',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'right',
            color: '#323232',
            //whiteSpace : 'nowrap',
            wordBreak: 'break-word'
        },
        fontSecondary: {
            fontFamily: 'fontCirXxttReg',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'left',
            color: '#323232',

        },
        fontSylesPopUp: {
            fontFamily: 'fontCirXxttReg !important',
            fontSize: '14px',
            fontWeight: 'normal',
            fontStretch: 'normal',
            fontStyle: 'normal',
            lineHeight: '1.43',
            letterSpacing: '0.25px',
            textAlign: 'left',
            color: '#0c6cde !important',
            cursor: 'pointer',
        },
    })

)

const ExpandedTemplate = (props: any) => {

    const classes = useStyles();

    const {
        prev_values,
        ord_id,
        ord_ln_item_id
    } = props.data

    const { t } = useTranslation();

    return (

        <Grid container spacing={2} direction="column" className={classes.drawerColor}>
            {prev_values ?
                JSON.parse(JSON.parse(prev_values).json_agg).map((order: any) => {
                    return (
                        order.key !== "extra" && order.key !== "lst_mod_dt" ?
                            <Grid item xs container spacing={0} >
                                <Grid item xs={12} md={6} sm container className={classes.fontSecondary}>
                                    {t(order.key)}
                                </Grid>
                                <Grid item xs={12} md={6} sm container style={{ display: "block" }}>
                                    <Typography className={classes.fontSecRight}> Was {order.value}  </Typography>
                                    <Typography className={classes.fontSecRight1}>Now {props[order.key] ? props[order.key] : "None"}   </Typography>
                                </Grid>
                            </Grid>
                            : null
                    )
                })
                : null}
            <Divider />
            <Grid item xs container spacing={0} className={classes.fontSylesPopUp}>
                <Typography
                // onClick={() => {
                //     getOrderDetails({
                //         showOrderDetails: true,
                //         whichOrderId: ord_id,
                //         whichOrderLineItemId: ord_ln_item_id
                //     });
                // }}
                > View {ord_ln_item_id}</Typography>
            </Grid>
        </Grid>

    )
}

export default ExpandedTemplate;