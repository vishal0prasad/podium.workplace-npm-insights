import React from "react";
import "./Custom_Control_Delete_Alert_Dialog_Style.css";

const DeleteAlertTemplate = ({data}) => {
    return ( 
        <div className="tempContainer">
            <div className="tempItem">
                <label>Custom Control Sheet name</label>
                <p>{data.viewName}</p>
            </div>
            <div className="tempItem">
                <label>Description</label>
                <p>{data.viewDescription}</p>
            </div>
        </div>
     );
}
 
export default DeleteAlertTemplate;