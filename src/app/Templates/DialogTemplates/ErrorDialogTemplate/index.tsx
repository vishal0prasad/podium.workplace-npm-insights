import React from "react";

import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

import templateMetaData from "./metaData.json";

const useStyles = makeStyles(() => ({
    alert : {
        fontSize : '14px'
    },
    subText : {
        fontSize : "14px"
    }
}))

const ErrorDialogTemplate = () => {
    const classes = useStyles();
    return ( 
        <>
            <Alert severity="info" className={classes.alert}>
                {templateMetaData.infoText}
            </Alert>
            <p className={classes.subText}>
                {templateMetaData.warning}
            </p>
        </>
     );
}
 
export default ErrorDialogTemplate;