
export const Constants = {
  PAGE_NAME: "OrderLanding",
  APP_CODE: "TrackTrace",
};

export const landingComponents = {
  myHomeTabs: "MyHome_Tabs",

  masterControlSheetsTab: "MasterControlSheets_Tab",
  customControlSheetsTab: "CustomControlSheets_Tab",

  masterControlSheetsTable: "MasterControlSheets_Table",
  customControlSheetsTable: "CustomControlSheets_Table",

  upload: "Upload",
  activityLog: "ActivityLog",
};

export const DIALOG_NAMES = {
    delete : "deleteDialog",
    edit : "editDialog",
    error : "errorDialog"
}

export const DIALOG_MANDATORY_FIELDS = ["viewName","viewDescription"]

export const CUSTOMVIEW_SUCESS_ALERT_CONFIGS = {
  anchorOrigin:{
    vertical: 'top',
    horizontal: 'center'
  },
  autoHideDuration : 3000,
  message : "Custom Control Sheet's Saved",
  type : "success",
}