import {models} from "@udp-dataplatform/podiumuiengine"

export interface LandingPage  
{
   landing: [TabsMetadata, UploadMetadata, ActivityLogMetadata]

}

export interface TabsMetadata extends models.BaseProps<TabMetadata>
{
   metadata: Array<TabMetadata>
   showtab: boolean
   styles : object
}

export interface UploadMetadata extends models.BaseProps<object>
{

}


export interface TabMetadata extends models.BaseProps<object>
{
   metadata: [ TableMetadata ]
}

export interface TableMetadata extends models.BaseProps<object>
{
   method: string,
   dataURI: string,
   styles : object
}

export interface ActivityLogMetadata extends  models.BaseProps<object>
{
   metadata: {
      CollapseTemplateURI: object,
      ExpandedTemplateURI: object

   }
   dataURI: string,
   mutual_exclusive_expansion: boolean
   sort: boolean

}

