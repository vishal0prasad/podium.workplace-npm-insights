import { ApplicationConfig } from '../../configs/application'
import {apiUrls} from "../../configs/apiUrls"

export const uploadCSVMetaData = () => {
    return {
        "title": "Upload",
        "icon": "add",
        "showBtn": true,
        "showIcon": true,
        "parentClassName": "root",
        "className": "button",
        "metaActionData": {
            "onClick": {
                "dialog": {
                    "heading": "Import Dataset",
                    "description": "Upload data from an Excel spreadsheet or CSV file.",
                    "isToggle": true,
                    "bodyContentType": "form",
                    "bodyContent": {
                        "formObject": {
                            "layouts": [
                                {
                                    "objectName": "upload_file",
                                    "objectType": "form",
                                    "componentWidth": 12,
                                    "sections": [
                                        {
                                            "name" : "Upload",
                                            "componentWidth": 12,
                                            "fields" : [
                                                {
                                                    "apiName" : "files",
                                                    "type": "file",
                                                    "draggable": true,
                                                    "format": ".csv, .xls, .xlsx",
                                                    "readOnly" : false,
                                                    "mandatory" : true,
                                                    "label": "Choose a file or drag file here",
                                                    "componentWidth": 12,
                                                    "multiple": false
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "objectName": "fields",
                                    "objectType": "form",
                                    "componentWidth": 12,
                                    "sections": [
                                        {
                                            "name" : "Selections",
                                            "componentWidth": 6,
                                            "fields" : [
                                                {
                                                    "apiName" : "supplier",
                                                    "fieldType": "supplier",
                                                    "type": "select",
                                                    "plceholder": "Select supplier",
                                                    "label": "Supplier",
                                                    "readOnly" : false,
                                                    "mandatory" : true,
                                                    "mockData": [
                                                        {"key": "Infrabuild", "value": "infrabuild"},
                                                        {"key": "Stora", "value": "stora"}
                                                    ],
                                                    "componentWidth": 12,
                                                    "dataURI": ApplicationConfig.getUrl(apiUrls.uploadCSV.getSupplierOption.url, false)
                                                },
                                                {
                                                    "apiName" : "project",
                                                    "fieldType": "prj",
                                                    "type": "select",
                                                    "plceholder": "Select project",
                                                    "label": "Project",
                                                    "readOnly" : false,
                                                    "mandatory" : true,
                                                    "mockData": [
                                                        {"key": "Milan", "value": "milan"},
                                                        {"key": "Test1", "value": "test1"}
                                                    ],
                                                    "componentWidth": 12,
                                                    "dataURI": ApplicationConfig.getUrl(apiUrls.uploadCSV.getProjectsOption.url, false)
                                                },
                                                {
                                                    "apiName" : "category",
                                                    "fieldType": "trade",
                                                    "type": "select",
                                                    "plceholder": "Select category",
                                                    "label": "Category",
                                                    "readOnly" : false,
                                                    "mandatory" : true,
                                                    "mockData": [
                                                        {"key": "Test1", "value": "test1"},
                                                        {"key": "Test2", "value": "test2"}
                                                    ],
                                                    "componentWidth": 12,
                                                    "dataURI": ApplicationConfig.getUrl(apiUrls.uploadCSV.getcategoriesOption.url, false)
                                                }
                                            ]
                                        },
                                        {
                                            "name" : "Description",
                                            "componentWidth": 6,
                                            "fields" : [
                                                {
                                                    "apiName" : "description",
                                                    "type": "smallField",
                                                    "placeholder": "Description (Optional)",
                                                    "label": "Description (Optional)",
                                                    "readOnly" : false,
                                                    "componentWidth": 12,
                                                    "mandatory" : false,
                                                    "multiline" : true,
                                                    "rows" : "9",
                                                    "variant" :"outlined"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    },
                    "footerActions": {
                        "secondry": {
                            "title": "cancel",
                            "type": "button",
                            "className": "secondry",
                            "showBtn": true,
                            "showIcon": false
                        },
                        "primary": {
                            "title": "Upload Dataset",
                            "type": "button",
                            "className": "button",
                            "showBtn": true,
                            "showIcon": false,
                            "defaultBackground": "#c4c4c4",
                            "defaultTextColor": "#fff"
                        }
                    }
                }
            }
        }
    }
}