import DeleteAlertTemplate from "../../../Templates/DialogTemplates/DeleteAlertTemplate";
import ErrorDialogTemplate from "../../../Templates/DialogTemplates/ErrorDialogTemplate";

export const editDialogMetadeta = {
    dialog: {
        heading: "Edit Custom Control Sheet",
        description: "",
        isToggle: true,
        closeIcon: false,
        bodyContentType: "form",
        bodyContent: {
            onClick: (e: MouseEvent) => { },
            formObject: {
                layouts: [
                    {
                        objectName: "fields",
                        sections: [
                            {
                                name: "Description",
                                componentWidth: 12,
                                fields: [
                                    {
                                        label: "Custom Control Sheet name",
                                        type: "smallField",
                                        apiName: "viewName",
                                        placeholder: "Enter view name",
                                        mandatory: true,
                                        variant: "outlined"
                                    },
                                    {
                                        label: "Description",
                                        type: "smallField",
                                        apiName: "viewDescription",
                                        placeholder: "Enter view description",
                                        mandatory: true,
                                        multiline: true,
                                        rows: "4",
                                        variant: "outlined"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        },
        footerActions: {
            secondry: {
                title: "cancel",
                type: "button",
                className: "secondry",
                showBtn: true,
                showIcon: false
            },
            primary: {
                title: "save",
                showBtn: true,
                className: "button",
                type: "button",
                showIcon: false,
                defaultBackground: "#c4c4c4",
                defaultTextColor: "#fff"
            },
        }
    }
}


export const deleteDialogMetadeta = {
    dialog: {
        heading: "Confirm Delete?",
        description: "",
        isToggle: true,
        closeIcon: false,
        bodyContentType: "template",
        bodyContent: {
            template: DeleteAlertTemplate,
            templateProps: {
                data: ''
            }
        },
        footerActions: {
            secondry: {
                title: "cancel",
                type: "button",
                className: "secondry",
                showBtn: true,
                showIcon: false
            },
            primary: {
                title: "delete",
                showBtn: true,
                className: "button",
                type: "button",
                showIcon: false,
                defaultBackground: "#c4c4c4",
                defaultTextColor: "#fff"
            },
        }
    }
}

export const loadingDialogMetaData = {
    dialog: {
        heading: "",
        description: "",
        isToggle: "false",
        bodyContentType: "spinner",
        bodyContent: {
            text: "Loading"
        },
        footerActions: {}
    }
}


export const errorDialogMetaData = {
    dialog: {
        heading: "Cannot save",
        description: "",
        isToggle: true,
        closeIcon: false,
        bodyContentType: "template",
        bodyContent: {
            template: ErrorDialogTemplate,
        },
        footerActions: {
            primary: {
                title: "back",
                showBtn: true,
                className: "button",
                type: "button",
                showIcon: false,
                defaultBackground: "#c4c4c4",
                defaultTextColor: "#fff"
            },
        }
    }
}
