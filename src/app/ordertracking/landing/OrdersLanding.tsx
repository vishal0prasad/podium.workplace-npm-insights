//@ts-nocheck
import React from "react";
import { useState } from "react";

import Grid from "@material-ui/core/Grid";

import { GenericWrapperComponent } from "@udp-dataplatform/podiumuiengine";

import landingPageMetadata from "../../data/orderLandingTabsMetadata.json";
import {
  ActivityLogMetadata,
  TabsMetadata,
  TabMetadata,
  LandingPage,
  UploadMetadata,
} from "./models/landingMetadata";
import { LandingProps } from "./models/landingProps";
import { apiUrls } from "../../configs/apiUrls";
import ActivityLogCollapseTemplate from "../../Templates/ActivityLog/CollapseTemplate";
import ActivityLogExpandedTemplate from "../../Templates/ActivityLog/ExpandedTemplate";
import { tableActionTemplate } from "../../Templates/TableActionTemplate/actionTemplate";
import { Constants, landingComponents, DIALOG_NAMES } from "./constants";
import { ApplicationConfig } from "../../configs/application";
import { uploadCSVMetaData } from "./uploadCSVMetaData";
import DialogsContainerWrapper from "./components/DialogContainer";
import { tabsStyles } from "../landing/styles/landingStyles";
import { uploadStyles } from "../landing/styles/uploadStyles";
import './styles/custom.css';

import { getCustomViewRoute } from "../functions";

export let token: string = "";

let pageMetadata: LandingPage = landingPageMetadata;

const OrdersLanding = (props: LandingProps) => {

  const classes = uploadStyles();
  const [dialogStates, setDialogStates] = useState({
    open : false,
    viewId : '',
    name : ''
  })
  
  token = props.token;

  /**
   * Function to handle the edit custom controlsheet click
   *  To make it accessible, It is declared globally
   */
   ((glob) => {
    glob.csEditFunction = (e, rowId) => {
      e.stopPropagation();
      setDialogStates((prev) => ({
        ...prev,
        open: true,
        name: DIALOG_NAMES.edit,
        viewId: rowId
      }))
    };
  })(window);

  /**
   * Function to handle the delete custom controlsheet click
   * To make it accessible, It is declared globally
   */
   ((glob) => {
    glob.csDeleteFunction = (e, rowId) => {
      e.stopPropagation();
      setDialogStates((prev) => ({
        ...prev,
        open: true,
        name: DIALOG_NAMES.delete,
        viewId: rowId
      }))
    };
  })(window);

  pageMetadata.landing.forEach((comp) => {
    switch (comp.id) {
      case landingComponents.myHomeTabs:
        comp.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.myHomeTabs}`;
        (comp as TabsMetadata).metadata.forEach((tabComp) => {
          switch (tabComp.id) {
            case landingComponents.masterControlSheetsTab:
              tabComp.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.masterControlSheetsTab}`;
              tabComp.metadata.forEach((tabCompElement) => {
                switch (tabCompElement.id) {
                  case landingComponents.masterControlSheetsTable:
                    tabCompElement.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.masterControlSheetsTable}`;
                    tabCompElement.dataURI = ApplicationConfig.getUrl(
                      apiUrls.userAppPrefernece.masterControlSheets.url,
                      true
                    );
                    tabCompElement.metadata.table.link_function = getCustomViewRoute
                    break;
                }
              });

              break;
            case landingComponents.customControlSheetsTab:
              tabComp.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.customControlSheetsTab}`;
              tabComp.metadata.forEach((tabCompElement) => {
                switch (tabCompElement.id) {
                  case landingComponents.customControlSheetsTable:
                    tabCompElement.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.customControlSheetsTable}`;
                    tabCompElement.dataURI = ApplicationConfig.getUrl(
                      apiUrls.userAppPrefernece.customControlSheets.url,
                      true
                    );

                    tabCompElement.metadata.table.link_function = getCustomViewRoute

                    /**
                     * Only add template when it is required from the metadata
                     */
                    if (tabCompElement.metadata.table.tableActionsEnable) {
                      const templateFunc = (text, row, col) => {
                        return tableActionTemplate(text, row, col, tabCompElement.metadata.table);
                      }

                      tabCompElement.metadata.table.template = templateFunc
                    }

                    break;
                }
              });

              break;
          }
        })

        break;
      case landingComponents.upload:
        let uploadComp = comp as UploadMetadata;
        uploadComp.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.upload}`;
        uploadComp.dataURI = ApplicationConfig.getUrl(
          apiUrls.uploadCSV.uploadData.url
        );
        uploadComp.metadata = uploadCSVMetaData();

        break;
      case landingComponents.activityLog:
        let activityComp = comp as ActivityLogMetadata;
        activityComp.id = `${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.activityLog}`;
        activityComp.dataURI = ApplicationConfig.getUrl(
          apiUrls.order.historyView.url,
          true
        );
        activityComp.metadata = {
          CollapseTemplateURI: ActivityLogCollapseTemplate,
          ExpandedTemplateURI: ActivityLogExpandedTemplate,
        };

        break;
    }
  });

  return (
    <div>
      <DialogsContainerWrapper
        id={`${Constants.APP_CODE}_${Constants.PAGE_NAME}_${landingComponents.customControlSheetsTable}`}
        open={dialogStates.open}
        viewId={dialogStates.viewId}
        setDialogStates={setDialogStates}
        name={dialogStates.name}
      />

      <Grid item xs={12} container spacing={3}>
        {pageMetadata.landing.map((item, index) =>
          index === 0 ? (
            <Grid item xs={item.width}>
              <GenericWrapperComponent
                item={item}
                key={index}
              ></GenericWrapperComponent>
            </Grid>
          ) : (
            <div className={classes.uploadBtn}>
              <GenericWrapperComponent
                item={item}
                key={index}
              ></GenericWrapperComponent>
            </div>
          )
        )}
      </Grid>
    </div>
  );
};

export default OrdersLanding;
