import { podiumtheme } from "@udp-dataplatform/podiumuiengine";

export const tabsStyles = (theme: podiumtheme) => {
  return{
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.common.whisper,
      paddingTop: "4rem",
    },
    tabs: {
      borderBottom: `1px solid ${theme.palette.common.alto}`,
      marginLeft: "50px",
      marginBottom: "-20px",
      marginTop: "-10px",
      marginRight: "50px",
    },
    tab: {
      textTransform: "none",
      fontFamily: "fontCirStdMedium",
      fontSize: "14px",
      fontStretch: "normal",
      fontStyle: "normal",
      lineHeight: "1.43",
      letterSpacing: "0.1px",
      textAlign: "left",
      color: theme.palette.common.dune,
      paddingLeft: "0px",
    },
    typography: {
      fontFamily: "fontCirStdMedium, fontCirXxttReg",
    },
    indicatorColor: {
      background: theme.palette.common.java,
    },
    tabpanel: {
      marginLeft: "25px",
      marginTop: "5px",
      marginRight: "25px",
    },
    tabIndicator: {
      background: theme.palette.common.java,
      height: "2px",
    },
  }
};
