import Mustache from "mustache";
import { HttpService } from "@udp-dataplatform/podiumuiengine";
import { ApplicationConfig } from "../../configs/application";

const http = new HttpService();

/**
 * Function to structure the full path for api
 * @param path unstructured path 
 * @param values values to replace with placeholders
 * @returns Structured path
 */
const transpilePath = (path:string, values:{} | null = null) => {
    return Mustache.render(ApplicationConfig.getUrl(
            path,
            true
            ), values && values);
}

/**
 * 
 * @param path Api endpoint(Unstructured)
 * @param data Payload for API
 * @returns Promise. Api response
 */
export const patchCustomViewData = (path:string, data:object | string) => {
    data = JSON.stringify(data);
    return http.patch(transpilePath(path), data)
}

/**
 * 
 * @param path Api endpoint(Unstructured)
 * @param viewId Id to delete the view
 * @returns Promise.
 */
export const deleteCustomView = (path: string, viewId:number|string) => {
    return http.delete(transpilePath(path, { viewId }));
}