import React from "react";
import { useState, useEffect } from "react";

import { connect } from "react-redux";

import { DynamicDialog, AlertWrapperComponent, store, actions} from "@udp-dataplatform/podiumuiengine";


import { DIALOG_NAMES, CUSTOMVIEW_SUCESS_ALERT_CONFIGS, DIALOG_MANDATORY_FIELDS } from "../constants";
import {patchCustomViewData, deleteCustomView} from "../ApiServices";
import { editDialogMetadeta, deleteDialogMetadeta, loadingDialogMetaData,errorDialogMetaData } from "../metadata/actionsDialogMetaData";
import { apiUrls } from "../../../configs/apiUrls";

const dialogMetaObject:any = {
    [DIALOG_NAMES.edit]: editDialogMetadeta,
    [DIALOG_NAMES.delete]: deleteDialogMetadeta,
    [DIALOG_NAMES.error]: errorDialogMetaData,
}

const DialogsContainer = ({ viewId, name, open, setDialogStates, customViewsData, ...props}:any) => {
    let propsData: any = {
        primaryAction: null,
        primaryDisable: false,
        dialogData: dialogMetaObject[name]
    };

    const [propsDataState, setPropsDataState] = useState(propsData);
    const [values, setValues] = useState({});
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);

    const setInitialValues = () => {
        if (customViewsData) {
            if (customViewsData.tableData) {
                const viewsData = customViewsData.tableData.filter((view: any) => {
                    return view.id == viewId
                })

                setValues(viewsData[0]);
            }
        }
    }

    useEffect(() => {
        setInitialValues();
    }, [viewId])


    /**
     * Change dialog Data as per state change
     */
    useEffect(() => {
        setPropsDataState((prev) => ({
            ...prev,
            dialogData: dialogMetaObject[name]
        }))
    }, [name])


    /**
    * Handle input changes of the form used inside dialog.
    * This will gets triggered on every input change
    * @param e Form event to handle inputs
    */
    const handleChange = (e: any) => {
        const {name,value} = e.target;

        setValues((prevValues:any) => ({
            ...prevValues,            
            [name]: value
        }))


        /**
         * Check for empty values
         */
        DIALOG_MANDATORY_FIELDS.forEach((field) => {
            (field === name) &&
                setPropsDataState((prevState: any) => ({
                    ...prevState,
                    primaryDisable: !value ? true : false
                }))
        })

    }

    /**
     * Primary action for the delete dialog.
     */
    const deleteDialogPrimaryAction = async () => {
        setLoading(true)

        /**
         * Get and transpile the deleteView api url 
         */
        const path = apiUrls.userAppPrefernece.deleteView.url

        await deleteCustomView(path, values.id);

        /**
         * Toggle refreshTable state to render the new data
         */
        store.dispatch(
            actions.TableActions.updateRefereshTableFlag(props.id)
        );

        setLoading(false);
        setDialogStates((prev) => ({
            ...prev,
            open:false
        }))
    } 


    /**
     * Primary action for the edit dialog.
     */
    const editDialogPrimaryAction = async () => {
        setLoading(true)

        const {id, viewName, viewDescription}  = values;

        const path = apiUrls.userAppPrefernece.saveComponent.url
        
        try {
            /**
             * Making the patch request
             */
            await patchCustomViewData(path, { id, viewName, viewDescription });

            setDialogStates((prev) => ({
                ...prev,
                open: false
            }))

            setShow(true);
            
            /**
            * Toggle refreshTable state to render the new data
            */
            store.dispatch(
                actions.TableActions.updateRefereshTableFlag(props.id)
            );

        } catch (error) {
            const status = error.response.status;

            switch(status){
                case 400 : 
                    /**
                     * Show error dialog on 400 status code
                     */
                    setDialogStates((prev) => ({
                        ...prev,
                        name: DIALOG_NAMES.error
                    }))
            }
        }

        setLoading(false);
    }


    /**
     * Primary action for the error dialog.
     */
    const errorDialogPrimaryAction = () => {
        setDialogStates((prev) => ({
            ...prev,
            name: DIALOG_NAMES.edit
        }))
    }

    /**
     * Handle dialog on toggle(close) 
     */
    const handleToggle = () => {
        
        
        setDialogStates((prev) => ({
            ...prev,
            open: false
        }))

        /**
         * Set the values to initials, If user hides the dialog
         */
        setInitialValues();
        
        /**
         * Set propsData to inital state when dialog is closed
         */
        setPropsDataState(propsData);
        
    }

    /**
     * Change metadata as per required dialog name
     */
    if (propsData.dialogData) {

        /**
         * Make template changes to the metadata 
         */
        switch (name) {
            case DIALOG_NAMES.delete:

                /**
                 * Make template changes to the metadata 
                 */
                if (propsData.dialogData.dialog.bodyContentType === "template") {
                   
                    /**
                     * Add props for delete dialog template
                     */
                    propsData.dialogData.dialog.bodyContent.templateProps = {
                        ...propsData.dialogData.dialog.bodyContent.templateProps,
                        data: values
                    }
                }

                /**
                 * Set primaryAction for delete Dialog
                 */
                propsDataState.primaryAction = deleteDialogPrimaryAction

                break;

            case DIALOG_NAMES.edit:
                /**
                * Set primaryAction for edit Dialog
                */
                propsDataState.primaryAction = editDialogPrimaryAction
                
                break;
            
            case DIALOG_NAMES.error:
                /**
                * Set primaryAction for edit Dialog
                */
                propsDataState.primaryAction = errorDialogPrimaryAction

        }
    }        

    return ( 
        <>
           
            <AlertWrapperComponent
                show={show}
                anchorOrigin={CUSTOMVIEW_SUCESS_ALERT_CONFIGS.anchorOrigin}
                autoHideDuration={CUSTOMVIEW_SUCESS_ALERT_CONFIGS.autoHideDuration}
                message={CUSTOMVIEW_SUCESS_ALERT_CONFIGS.message}
                type={CUSTOMVIEW_SUCESS_ALERT_CONFIGS.type}
                handleClose={() => setShow(false)}
            />

        {
            open &&
                <DynamicDialog
                    open={open ? true : false}
                    toggleDialog={handleToggle}
                    primaryAction={propsDataState.primaryAction}
                    values={values}
                    primaryDisable={propsDataState.primaryDisable}
                    handleChange={handleChange}
                    maxWidth="sm"
                    dialogData={
                        loading ? loadingDialogMetaData : propsDataState.dialogData || propsData.dialogData
                    }
                    
                />
        }
        
        </>
     );
}
 
const mapStateToProps = (state: any, ownProps: any) => {
    return {
        customViewsData: state[`${ownProps.id}_table`],
    }
}
export default connect(mapStateToProps)(DialogsContainer);