import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';

import { ApplicationConfig } from '../../../configs/application'

const Order : any = (props : typeof Order.propTypes) => {
    const { id, componentWidth, Component, componentData } = props;

    let { MetaData, DataURI, method, DataHistoryURI, OrderDetailsDataURI, ExpAppDataURI ,CollapseTemplateURI, ExpandedTemplateURI, mutual_exclusive_expansion, sort } = componentData;

    DataURI = ApplicationConfig.getUrl(DataURI, true);
    DataHistoryURI = ApplicationConfig.getUrl(DataHistoryURI, false);
    OrderDetailsDataURI = ApplicationConfig.getUrl(OrderDetailsDataURI, false);
    ExpAppDataURI = ApplicationConfig.getUrl(ExpAppDataURI, true);

    return (
        <Grid item xs={componentWidth}>
            <Component id={id} metaDataURL={MetaData} dataURL={DataURI} dataHistoryURI={DataHistoryURI} ordersDetailsDataURL={OrderDetailsDataURI} expAppDataURL={ExpAppDataURI} method={method}
             CollapseTemplateURI={CollapseTemplateURI} ExpandedTemplateURI={ExpandedTemplateURI} mutual_exclusive_expansion={mutual_exclusive_expansion} 
             sort={sort} />
        </Grid>
    )
}

Order.propTypes = {
    componentName: PropTypes.string.isRequired,
    componentWidth: PropTypes.number.isRequired,
    Component: PropTypes.elementType.isRequired,
    componentData: PropTypes.object.isRequired
}

export default Order
