//@ts-nocheck
import React from "react";
import { useParams } from "react-router-dom";

import Grid from "@material-ui/core/Grid";

import { TabsWrapperComponent, UnderConstructionWrapperComponent } from "@udp-dataplatform/podiumuiengine";

import notificationMetadata from "../../data/notifications/notificationCentreMetadata.json";
import { NotificationProps, TabType } from "./models/notificationProps";
import { apiUrls } from "../../configs/apiUrls";
import { Constants, landingComponents,PARAM_TYPE_NUMBER } from "./constants";
import { ApplicationConfig } from "../../configs/application";
import { store } from "@udp-dataplatform/podiumuiengine";

const NotificationsLanding = (props: NotificationProps) => {

    const state: IExperienceModuleState =
        store.getState() as IExperienceModuleState;
    const appCode = state?.experience?.appCode;
    const notificationLandingMetadata = notificationMetadata;
    // initialize metadata
    
    notificationLandingMetadata.id = `${appCode}_${Constants.PAGE_NAME}_${landingComponents.notificationTabs}`;

    /**
     * rule_type as defined in DB
     *  0: Watch
     *  1: Reminder
     */
    notificationLandingMetadata.metadata.forEach((tabComp) => {
        switch (tabComp.id) {
            case landingComponents.reminderTab:
                tabComp.id = `${appCode}_${Constants.PAGE_NAME}_${landingComponents.reminderTab}`;
                tabComp.metadata.forEach((tabCompElement) => {
                    switch (tabCompElement.id) {
                        case landingComponents.reminderTable:
                            tabCompElement.id = `${appCode}_${Constants.PAGE_NAME}_${landingComponents.reminderTable}`;
                            tabCompElement.dataURI = ApplicationConfig.getUrl(apiUrls.notification.notificationCentre.url, true);
                            tabCompElement.metadata.table.requestData.query = `app_code=${appCode?.toLowerCase()}&rule_type=1`;
                            break;
                    }
                });
                break;
            case landingComponents.watchTab:
                tabComp.id = `${appCode}_${Constants.PAGE_NAME}_${landingComponents.watchTab}`;
                tabComp.metadata.forEach((tabCompElement) => {
                    switch (tabCompElement.id) {
                        case landingComponents.watchTable:
                            tabCompElement.id = `${appCode}_${Constants.PAGE_NAME}_${landingComponents.watchTable}`;
                            tabCompElement.dataURI = ApplicationConfig.getUrl(apiUrls.notification.notificationCentre.url, true);
                            tabCompElement.metadata.table.requestData.query = `app_code=${appCode?.toLowerCase()}&rule_type=0`;
                            break;
                    }
                });
                break;
        }
    });

    const { type } = useParams();
    const tabToRender = TabType[type];
    // currently only checks if type is reminder/watcher and renders else shows default page
    return (
        <>
            <Grid item xs={12} container spacing={3}>
                <Grid item xs={12}>
                    {typeof tabToRender === PARAM_TYPE_NUMBER ? (
                        <TabsWrapperComponent
                            name={notificationLandingMetadata.name}
                            metadata={notificationLandingMetadata.metadata}
                            tabValue={tabToRender}
                        />
                    ) : (
                        <UnderConstructionWrapperComponent />
                    )}
                </Grid>
            </Grid>
        </>
    );
};

export default NotificationsLanding;
