export interface NotificationProps {
    token: string;
}

export enum TabType {
    "reminder",
    "watcher",
}
