import { models } from "@udp-dataplatform/podiumuiengine";

export interface LandingPage {
    landing: [TabsMetadata];
}

export interface TabsMetadata extends models.BaseProps<TabMetadata> {
    metadata: Array<TabMetadata>;
    showtab: boolean;
    styles: object;
}
export interface TabMetadata extends models.BaseProps<object> {
    metadata: [TableMetadata];
}

export interface TableMetadata extends models.BaseProps<object> {
    method: string;
    dataURI: string;
    styles: object;
}
