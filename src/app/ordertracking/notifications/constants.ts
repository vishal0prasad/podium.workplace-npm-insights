export const Constants = {
    PAGE_NAME: "NotificationCentre",
    APP_CODE: "TrackTrace",
};

export const landingComponents = {
    notificationTabs: "Notification_Tabs",

    reminderTab: "Reminder_Tab",
    reminderTable: "Reminder_Table",

    watchTab: "Watch_Tab",
    watchTable: "Watch_Table",
};

export const PARAM_TYPE_NUMBER = "number";
