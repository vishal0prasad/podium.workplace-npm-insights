import Mustache from "mustache";
import { appLinks } from "../configs/appLinks";


export const getCustomViewRoute = (id: string) => {
  return  Mustache.render(appLinks.orders.customView,{viewId:id})
};


export const getOrderDetailsRoute= (orderId : number ,lineItemId: number) =>{
  return  Mustache.render(appLinks.orders.orderDetails,{orderId:orderId, lineItemId:lineItemId })
}

export const getOrderRoute= (orderId : number) =>{
  return  Mustache.render(appLinks.orders.order,{orderId:orderId })
}

