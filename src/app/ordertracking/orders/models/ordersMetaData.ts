import { models } from "@udp-dataplatform/podiumuiengine";
import { FC } from "react";

export interface OrdersPage {
  pageElement: {
    componentList: Array<ComponentMetaData>;
  };
}

export interface ComponentMetaData {
  id: string;
  componentName: string;
  componentWidth: number;
  Component: FC;
  componentData: ComponentData;
}

export interface ComponentData {
  MetaData: object;
  MetaDataFormObject: object;
  MetaDataPrintable: object;
  TableDataURI: string;
  GanttDataURI: string;
  CalendarDataURI: string;
  OrderDetailsDataURI: string;
  CustomiseSaveURI: string;
  CustomiseAppURI: string;
  CustomiseDefineOrderViewURI: string;
  CustomiseDefineOrderLineItemsURI: string;
  ParentDefineOrderURI: string;
  CustomiseViewsPageViewURI: string;
  GetAllExpAppURI: string;
  GetSelectedExpAppMenuData: string;
  externalReferenceURI: string;
  URL: object;
  Arg: object;
  method: string;
}
