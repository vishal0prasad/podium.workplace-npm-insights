export interface ordersStatusTrackingProps {
    id: string,
    tableMetaDataURLObject: object,
    formObjectMetaDataURLObject: object,
    ganttMetaDataURLObject: object
    calendarMetaDataURLobject: object,
    getSelectedExpAppMenuData: object,
    tableDataURL: string,
    ganttDataURL: string, 
    calendarDataURL: object,
    orderDetailsDataURL: string,
    customiseSaveURIstring: string, 
    customiseAppURI: string,
    customiseDefineOrderViewURI: string,
    customiseDefineOrderLineItemsURI: string,
    customiseViewsPageViewURI: string,
    externalReferenceURI: string,
    getAllExpAppURI: string,
    method: string
}