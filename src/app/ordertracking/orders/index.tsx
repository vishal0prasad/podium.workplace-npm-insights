import React, { FC } from 'react';
import { Route } from "react-router-dom"

import Grid from '@material-ui/core/Grid';

import OrdersStatusTrackingMetaData from "../../data/ordersStatusTracking.json";
import ObjectFormMetaData from "../../data/formObjectMetaData.json";
import PrintableMetaData from "../../data/printableWrapperMetadata.json";
import { apiUrls } from "../../configs/apiUrls"
import OrdersStatusTracking from './components/ordersStatusTracking';
import { Constants } from './constants';
import { OrdersPage, ComponentMetaData, ComponentData } from './models/ordersMetaData'

const ordersPageMetaData: OrdersPage = {
    pageElement: {
        componentList: [
            {
                id: `${Constants.APP_CODE}_${Constants.PAGE_NAME}_OrderStatus`,
                componentName: "Order Status",
                componentWidth: 12,
                Component: OrdersStatusTracking,
                componentData: {
                    MetaData: OrdersStatusTrackingMetaData,
                    MetaDataFormObject: ObjectFormMetaData,
                    MetaDataPrintable: PrintableMetaData,
                    TableDataURI: apiUrls.order.view.url,
                    GanttDataURI: apiUrls.order.ganttView.url,
                    CalendarDataURI: apiUrls.order.calendarView.url,
                    OrderDetailsDataURI: apiUrls.order.list.url,
                    CustomiseSaveURI: apiUrls.userAppPrefernece.saveComponent.url,
                    CustomiseAppURI: apiUrls.userAppPrefernece.appCode.url,
                    CustomiseDefineOrderViewURI: apiUrls.order.defineView.url,
                    CustomiseDefineOrderLineItemsURI: apiUrls.order.defineViewLineItems.url,
                    ParentDefineOrderURI: apiUrls.order.defineParentView.url,
                    CustomiseViewsPageViewURI: apiUrls.userAppPrefernece.pageView.url,
                    GetAllExpAppURI: apiUrls.experience.data.url,
                    GetSelectedExpAppMenuData: apiUrls.experience.menu.url,
                    externalReferenceURI: apiUrls?.order?.filter?.url,
                    URL: {},
                    Arg: {},
                    method: 'get'
                },
            }
        ]
    }
};

const Orders: FC = () => {
    return (



        <Grid container spacing={3}>
            {ordersPageMetaData.pageElement.componentList.map(({ id, componentWidth, Component, componentData }: ComponentMetaData, index) => {
                const { MetaData, MetaDataFormObject, MetaDataPrintable, TableDataURI, GanttDataURI, CalendarDataURI, OrderDetailsDataURI, CustomiseSaveURI, CustomiseAppURI, CustomiseDefineOrderViewURI, CustomiseDefineOrderLineItemsURI, CustomiseViewsPageViewURI, GetAllExpAppURI, GetSelectedExpAppMenuData, method, externalReferenceURI, ParentDefineOrderURI }: ComponentData = componentData;
                return (
                    <Grid item xs={componentWidth} key={index}>
                        <Component
                            id={id}
                            tableMetaDataURLObject={MetaData}
                            formObjectMetaDataURLObject={MetaDataFormObject}
                            printableMetaDataURLObject={MetaDataPrintable}
                            ganttMetaDataURLObject={MetaData}
                            calendarMetaDataURLObject={MetaData}
                            getSelectedExpAppMenuData={GetSelectedExpAppMenuData}
                            tableDataURL={TableDataURI}
                            ganttDataURL={GanttDataURI}
                            calendarDataURL={CalendarDataURI}
                            orderDetailsDataURL={OrderDetailsDataURI}
                            customiseSaveURI={CustomiseSaveURI}
                            customiseAppURI={CustomiseAppURI}
                            customiseDefineOrderViewURI={CustomiseDefineOrderViewURI}
                            customiseDefineOrderLineItemsURI={CustomiseDefineOrderLineItemsURI}
                            parentDefineOrderURI={ParentDefineOrderURI}
                            customiseViewsPageViewURI={CustomiseViewsPageViewURI}
                            getAllExpAppURI={GetAllExpAppURI}
                            externalReferenceURI={externalReferenceURI}
                            method={method}
                        />
                    </Grid>
                )
            })}
        </Grid>

    )
}

export default Orders;