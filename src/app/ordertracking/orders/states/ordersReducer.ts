import { OrdersActionTypes } from "./ordersActions";

export interface OrdersState {
    globalSearch: string;
    query: string;
}

const initState: OrdersState = {
    globalSearch: '',
    query: ''
}

const ordersReducer = (state = initState, action: any) => {
    switch (action.type) {

        case OrdersActionTypes.updateOrdersQuery:
            return {
                ...state,
                 ...action.payload,
              };

    }

}