//@ts-nocheck
import React from "react";
import { Dialog } from '@material-ui/core';

const TestComponent = (props:any) =>{
    const {open, onClose} = props
    return(
        <Dialog onClose = {onClose} open={open}>
            <button onClick={() => window.alert("Hello")}>Click Me!</button>
        </Dialog>
    )
}

export default TestComponent