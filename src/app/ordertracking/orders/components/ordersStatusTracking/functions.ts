import OrdersStatusTrackingMetaData from "../../../../data/ordersStatusTracking.json";
import { store, actions } from "@udp-dataplatform/podiumuiengine";
declare const window: any;
 let checkboxConfigName = "checkbox-config";

 export const functionName = ()=>{}

 export interface ICheckBoxConfig {
  lineItems:string[],
  parentItems:string[],
  expandedRows:string[],
}
let initialCheckboxConfig = {
  lineItems: [],
  parentItems: [],
  expandedRows: [],
};
 const storeCheckboxConfig = (key: string, value: ICheckBoxConfig) => {
  try {
    window.localStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    console.log(error);
  }
};

 const getCheckboxConfig = (key: string) => {
  try {
    const item = window.localStorage.getItem(key);
    return item ? JSON.parse(item) : initialCheckboxConfig;
  } catch (error) {
    return initialCheckboxConfig;
  }
};

const differenceWith = (a1: string[], a2: string[]) => {
  return a2.filter((d:string) => !a1.includes(d))
};

const uniq = (items: string[]) => {
  return Array.from(new Set(items));
};

 const handleGroupCheckboxChange = (
  e: Event,
  row: any,
  col: any
) => {
  const input = e.target as HTMLInputElement
  const childCheckboxes = document.querySelectorAll(`[parent="${row.id}"]`);
  childCheckboxes.forEach((element: any) => {
    element.checked = input.checked ? true : false;
  });
  const checkboxConfigs = getCheckboxConfig(checkboxConfigName);
  const allActiveParentCheckboxes = Object.assign(
    [],
    checkboxConfigs.parentItems
  );
  const allActiveParentLineItems = Object.assign([], checkboxConfigs.lineItems);
  const itemId = input.getAttribute("id");
  const lineItemsArray = row.items.map((i: any) => i.ord_ln_item_id?i.ord_ln_item_id.toString():i.ord_ln_item_id);
  if (input.checked) {
    allActiveParentCheckboxes.push(itemId);
    let uniqueParentItems = uniq(allActiveParentCheckboxes);
    storeCheckboxConfig(checkboxConfigName, {
      ...getCheckboxConfig(checkboxConfigName),
      parentItems: uniqueParentItems,
      lineItems: [...allActiveParentLineItems, ...lineItemsArray],
    });
  } else {
    if (allActiveParentCheckboxes.includes(itemId)) {
      const filterRecords = allActiveParentCheckboxes.filter(function (
        item: any
      ) {
        return item !== itemId;
      });
      const filteredLineItems = differenceWith(
        lineItemsArray,
        allActiveParentLineItems,
      );
      storeCheckboxConfig(checkboxConfigName, {
        ...getCheckboxConfig(checkboxConfigName),
        parentItems: filterRecords,
        lineItems: filteredLineItems,
      });
    }
  }
};

 const handleHeaderCheckboxChange = (
  checkboxHeaderElementVisible: boolean
) => {
  const headerCheckbox = document.querySelector("#header_checkbox");
  if (checkboxHeaderElementVisible) {
    headerCheckbox?.addEventListener("change", (e: Event) => {
      const target = e.target as HTMLInputElement
      const childCheckboxes = document.querySelectorAll(
        `[name="dhx_checkbox"]`
      );
      childCheckboxes.forEach((element: any) => {
        element.checked = target.checked ? true : false;
      });
    });
  }
};

const getCheckboxColumn = (
  header_id: string,
  fieldName: string,
  column_options: any
) => {
  const colConfig = {
    id: header_id,
    fieldName: fieldName,
    ...column_options,
    width: 50,
    minWidth: 50,
    header: [
      {
        text: "<input type='checkbox' id='header_checkbox' name='dhx_checkbox' class='dhx_checkbox'>",
      },
    ],
    template: (text: any, row: any, col: any) => {
      return null;
    },
    htmlEnable: true,
    customFields: true,
    sortable: false,
    draggable: false,
  };
  return colConfig;
};

const getOrderColumnConfig = (
  headerId: string,
  fieldName: string,
  column_options: any
) => {
  const colConfig = {
    id: headerId,
    fieldName: fieldName,
    ...column_options,
    width: 120,
    minWidth: 104,
    header: [
      {
        text: `<span class='headerTextSpacing'>Order#</span>`,
      },
    ],

    template: (text: any, row: any, col: any) => {
      let checkboxInput = "";
      if (
        OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn.enabled
      ) {
        const checkboxConfigs = getCheckboxConfig(checkboxConfigName);
        const allActiveLineItems = Object.assign([], checkboxConfigs.lineItems);
        let checked = false;
        if (
          allActiveLineItems.includes(
            row.ord_ln_item_id ? row.ord_ln_item_id.toString() : row.ord_ln_item_id
          )
        ) {
          checked = true;
        }
        if (checked) {
          checkboxInput = `<input type='checkbox' item-id='${row[fieldName]}'  parent='${row.parent}' name='dhx_checkbox' class='dhx_checkbox' checked=${checked}>`;
        } else {
          checkboxInput = `<input type='checkbox' item-id='${row[fieldName]}' parent='${row.parent}' name='dhx_checkbox' class='dhx_checkbox'>`;
        }
      }
      let groupTitle=text;
      if(!row.parentItem){
        groupTitle=`<a href="javascript:void()" class="dhx_order_line">${text}</a>`
      }
      return `<span class='dhx_checkbox_container'>${checkboxInput}${groupTitle}<span/>`;
    },
    htmlEnable: true,
    customFields: true,
    draggable: false,
  };
  return colConfig;
};

 const checkboxColumnConfigs = (column_options: any) => {
  let finalConfigs = [];
  let checkboxColumnConfig = null;
  let orderColumnConfig = null;
  if (OrdersStatusTrackingMetaData.table.checkbox) {
    if (
      OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn.enabled
    ) {
      checkboxColumnConfig = getCheckboxColumn(
        OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn
          .headerId,
        OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn
          .fieldName,
        column_options
      );
      finalConfigs.push(checkboxColumnConfig);
    }

    if (OrdersStatusTrackingMetaData.table.checkboxConfig.orderColumn.enabled) {
      orderColumnConfig = getOrderColumnConfig(
        OrdersStatusTrackingMetaData.table.checkboxConfig.orderColumn.headerId,
        OrdersStatusTrackingMetaData.table.checkboxConfig.orderColumn.fieldName,
        column_options
      );
      finalConfigs.push(orderColumnConfig);
    }
  }
  return finalConfigs;
};

 const appendCheckboxFields = (data: any) => {
  return data.map((val: any, index: number) => {
    val[
      OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn.headerId
    ] =
      val[
        OrdersStatusTrackingMetaData.table.checkboxConfig.checkboxColumn.fieldName
      ];
    val[
      OrdersStatusTrackingMetaData.table.checkboxConfig.orderColumn.headerId
    ] =
      val[
        OrdersStatusTrackingMetaData.table.checkboxConfig.orderColumn.fieldName
      ];
    return val;
  });
};

 const renderGroupCheckboxes = (groupName: string) => {
  const checkboxConfigs = getCheckboxConfig(checkboxConfigName);
  const allActiveParentCheckboxes = Object.assign(
    [],
    checkboxConfigs.parentItems
  );
  const group = `$group::${groupName}`;
  if (allActiveParentCheckboxes.includes(group)) {
    const parentCheckbox = document.querySelector(`[id="${group}"]`) as HTMLInputElement;
    if (parentCheckbox) {
      parentCheckbox.checked = true;
    }
  }
  return `${groupName}`
};

const clearCheckboxConfigs = () => {
  storeCheckboxConfig(checkboxConfigName,initialCheckboxConfig);
  return
};

const trackExpandedRows = (rowId: string,operation:string) => {
  const checkboxConfigs = getCheckboxConfig(checkboxConfigName);
  const storedExpandedRows = Object.assign([], checkboxConfigs.expandedRows);

  if(operation=="remove"){
  if (storedExpandedRows.includes(rowId)) {
    const uniqueExpandedRows = storedExpandedRows.filter(function (item: any) {
      return item !== rowId;
    });
    storeCheckboxConfig(checkboxConfigName, {
      ...getCheckboxConfig(checkboxConfigName),
      expandedRows: uniqueExpandedRows,
    });
  }
  return  
}
    storedExpandedRows.push(rowId);
    let uniqueExpandedRows = uniq(storedExpandedRows);
    storeCheckboxConfig(checkboxConfigName, {
      ...getCheckboxConfig(checkboxConfigName),
      expandedRows: uniqueExpandedRows,
    });
};

const checkIsRowExpanded = (rowId: string) => {
  const checkboxConfigs = getCheckboxConfig(checkboxConfigName);
  const storedExpandedRows = Object.assign([], checkboxConfigs.expandedRows);
 return storedExpandedRows.includes(rowId)
}

const prepareTreeViewData=(lodashChain:any) => {
  const tableMeta=OrdersStatusTrackingMetaData.table
  const parentRecords = [] as any;
  let childRecords =lodashChain
    .groupBy(tableMeta.treeGroupKey)
    .map((value:any, key:string) => {
      parentRecords.push({ id: key, ord_ln_item_id_cell_checkbox: key, ord_ln_item_id_checkbox_row: key,parentItem:true });
      return value.map((item:any) => {
        item.parent = key;
        return item;
      });
    })
    .value();
    if (tableMeta.checkbox && appendCheckboxFields && tableMeta.treeGroupKey) {
      childRecords = appendCheckboxFields(childRecords.flat(1))
    }
    const allRecords=[...parentRecords,...childRecords]
    return allRecords
}

const overrideTreeGridColumnConfigs = (
  columnConfig: any,
  treeGridOptions: any,
  columnOptions: any
) => {
  const tableMeta = OrdersStatusTrackingMetaData.table;
  try {
    const checkboxConfigs = checkboxColumnConfigs(columnOptions);
    columnConfig = [...checkboxConfigs, ...columnConfig];
    treeGridOptions = {
      ...treeGridOptions,
      checkbox: tableMeta.checkboxConfig.checkboxColumn.enabled || false,
      persistTreegrid: true,
      columns: columnConfig,
      checkboxProps: {
        name: "dhx_checkbox",
        disabled: false,
        onchange: handleGroupCheckboxChange,
      },
      groupTitleTemplate: function (groupName: string, items: any[]) {
        if (
          renderGroupCheckboxes &&
          tableMeta?.checkboxConfig?.checkboxColumn.enabled
        ) {
          return renderGroupCheckboxes(groupName);
        }
        return `${groupName}`;
      },
    };
    return treeGridOptions;
  } catch (error) {
    return columnConfig;
  }
};

export const handleCustomiseUpdate = (customHeaders:[], searchColumns: string, whichPageNum:number, id:string) => {
  store.dispatch(actions.TableActions.updateRequestData({ columns: customHeaders, refreshFlag: true, globalSearchColumns: searchColumns, pageNo: whichPageNum }, id))
}

export const treeViewCollection = {
  checkboxColumnConfigs,
  appendCheckboxFields,
  handleGroupCheckboxChange,
  storeCheckboxConfig,
  getCheckboxConfig,
  renderGroupCheckboxes,
  clearCheckboxConfigs,
  handleHeaderCheckboxChange,
  trackExpandedRows,
  checkIsRowExpanded,
  prepareTreeViewData,
  overrideTreeGridColumnConfigs
};

