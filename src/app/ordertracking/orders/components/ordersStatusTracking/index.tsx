//@ts-nocheck
import React, { useState } from "react";
import { Switch, Route } from "react-router-dom";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import { CustomiseWrapperComponent } from "@udp-dataplatform/podiumuiengine";
import { PrintableWrapperComponent } from "@udp-dataplatform/podiumuiengine";
import { SchedulerWrapperComponent } from "@udp-dataplatform/podiumuiengine";
import { GanttWrapperCompenent } from "@udp-dataplatform/podiumuiengine";
import { FilterWrapperComponent, LocationContext } from "@udp-dataplatform/podiumuiengine";
import { ToggleButtonComponent } from "@udp-dataplatform/podiumuiengine";
import { store, actions } from "@udp-dataplatform/podiumuiengine";
import {PageTitleWrapperComponent} from "@udp-dataplatform/podiumuiengine";
import { podiumContexts } from "@udp-dataplatform/podiumuiengine";

import {
  getParentQuantity,
  scales,
  modifyColumns,
} from "../ordersGanttView/functions";
import { getCustomViewRoute, getOrderDetailsRoute, getOrderRoute } from "../../../functions";
import PageControlComponent from "../PageControlComponent";
import TableViewPage from "../TableViewPage";
import TestComponent from "./TestComponent";
import {deleteFunc, editFunc, func, templateFunction} from "../../../../Templates/TableActionTemplate/tableTemplate"

import { ApplicationConfig } from "../../../../configs/application";
import { apiUrls } from "../../../../configs/apiUrls";

import { ordersStatusTrackingProps } from "../../models/ordersStatusTracking";
import { treeViewCollection, handleCustomiseUpdate } from "./functions";
import appRoutes from "../../../../configs/data/appRoutes";
import "../../styles/OrdersStatusTracking.css";
import {tableStyles} from "../../styles/tableStyles";
import "./custom.css"
import OrderDetails from "../orderdetails";

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={0}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
};


const OrderStatusTracking = ({
  id,
  tableMetaDataURLObject,
  printableMetaDataURLObject,
  formObjectMetaDataURLObject,
  tableDataURL,
  ganttMetaDataURLObject,
  ganttDataURL,
  calendarMetaDataURLObject,
  calendarDataURL,
  orderDetailsDataURL,
  customiseSaveURI,
  customiseAppURI,
  customiseDefineOrderViewURI,
  customiseDefineOrderLineItemsURI,
  parentDefineOrderURI,
  getAllExpAppURI,
  getSelectedExpAppMenuData,
  externalReferenceURI,
}: ordersStatusTrackingProps) => {
  tableDataURL = ApplicationConfig.getUrl(tableDataURL, false);
  ganttDataURL = ApplicationConfig.getUrl(ganttDataURL, false);
  calendarDataURL = ApplicationConfig.getUrl(calendarDataURL, false);
  orderDetailsDataURL = ApplicationConfig.getUrl(orderDetailsDataURL, false);
  customiseSaveURI = ApplicationConfig.getUrl(customiseSaveURI, true);
  customiseAppURI = ApplicationConfig.getUrl(customiseAppURI, true);
  customiseDefineOrderViewURI = ApplicationConfig.getUrl(
    customiseDefineOrderViewURI,
    false
  );
  customiseDefineOrderLineItemsURI = ApplicationConfig.getUrl(
    customiseDefineOrderLineItemsURI,
    false
  );
  parentDefineOrderURI = ApplicationConfig.getUrl(
    parentDefineOrderURI,
    false
  );
  getAllExpAppURI = ApplicationConfig.getUrl(getAllExpAppURI, true);
  getSelectedExpAppMenuData = ApplicationConfig.getUrl(
    getSelectedExpAppMenuData,
    true
  );
  externalReferenceURI = ApplicationConfig.getUrl(
    externalReferenceURI,
    false
  );

  
  const caseRequestURLs = {
    getAll: ApplicationConfig.getUrl(apiUrls.caseRequest.getAllRequests.url, true),
    getEnquiryTypes: ApplicationConfig.getUrl(apiUrls.caseRequest.getEnquiryTypes.url, true),
    getForm: ApplicationConfig.getUrl(apiUrls.caseRequest.getRequestForm.url, true),
    submit: ApplicationConfig.getUrl(apiUrls.caseRequest.submitForm.url, true),
    getDetails: ApplicationConfig.getUrl(apiUrls.caseRequest.getRequestDetails.url, true)
  }

  const [isCustomViewsOpen, setIsCustomViewOpen] = useState(false);
  const [currentTab, setCurrentTab] = useState(0);
  const [doubleView , setDoubleView] = useState(false); 
  
  const [open, setOpen] = React.useState(false);
  const [openCustomized, setOpenCustomized] = useState(false);
  const [customHeaders, setCustomHeaders] = useState([] as any[]);
  const [customLineHeaders, setCustomLineHeaders] = useState([] as any[]);
  

    const [x, setX] = useState()
    const [y, setY] = useState()
  
    // Hiding the Edit,Delete dropdown, showing dialog and setting X and Y for dialog
    const handleClickOpen = (e:any, row:string) => {
      setOpen(true);
      setX(e.clientX+"px")
      setY(e.clientY+"px")
      toggleShow(row)
    };
  
    // Close function for Dialog
    const handleClickClose = () => {
      setOpen(false)
    }

    const handleCustomisedClick =()=>{
     
      setOpenCustomized(true);
      let orderHeaders = store.getState().customise.customColumns;
      let orderLineHeaders = store.getState().customise.customLineColumns;
      setCustomHeaders(orderHeaders); 
      setCustomLineHeaders(orderLineHeaders);
    }
  
 
  // change tabs for table , gantt , calender
  const changeCurrentTab = (index) => {
    setCurrentTab(index);
  };


  const loadPrint = () => {
    store.dispatch(
      actions.TableActions.setUpdatePrint(true, id)
    )
  };

  const URL = {
    tenancyPrefix: ApplicationConfig.getUrl("", true),
    nonTenancyPrefix: ApplicationConfig.getUrl("", false),
  };

  const handleToggle = (open = !isCustomViewsOpen) => {
    setIsCustomViewOpen(open);
  };

  interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }

// Function to toggle menu dropdown
  const toggleShow = (id:string) =>{
    document.getElementById(id).classList.toggle("show"); 
  }
  
  // Function reference to the metadata template which assigns functions and calls the function which returns html
  /* const tableNotificationTemplate = (text:any, row:any,  col:any) => {
    func= toggleShow
    editFunc = handleClickOpen
    deleteFunc = handleClickOpen
    //Val gets the html string and this is returned to the template in TableView which then renders the html
    return val= templateFunction(text ,row ,col ,tableMetaDataURLObject, handleClickOpen) 
  }  */

  // Creating a metadata reference which will be called in TableView.tsx
  //tableMetaDataURLObject.table.template = tableNotificationTemplate

  const PageRouteComponents: any = {
    pageControlComponent: {
      id: PageControlComponent,
      props: { id: id, changeCurrentTab: changeCurrentTab },
    },
    orderDetailsComponent: {
      id: OrderDetails,
      props: { id: id },
    }
  };
  const classes = tableStyles();



  let doubleInitState = {
    parent : {
      id: id,
      requestData : {}
    },
    child : {
      id : `${id}_child`,
      requestData : {}
    },
    defineColumnsObject : {} 
  }


  const [columns, setColumns] = useState(doubleInitState);
  const updateColumns=(cols: any)=>{
    setColumns(cols)
  }
  const updateTableData = (data:any, dataId:any) => {
    if(dataId === id){
      setColumns ({
        ...columns,
        parent:{
          ...columns.parent,
          requestData : data
        }})    
    }else { 
      setColumns({
        ...columns,
        child:{
          ...columns.child,
          requestData : data
        }
      }) 
    }   
  }

  const [loc, setLocation] =useState("")

  const updatePrevLocation=(prevLoc)=> {
    setLocation(prevLoc)
  }

 
  tableMetaDataURLObject.table.dialog_linkfunction = getOrderDetailsRoute;
  tableMetaDataURLObject.table.dialog_parent_linkfunction = getOrderRoute;
  ganttMetaDataURLObject.gantt.dialog_linkfunction = getOrderDetailsRoute;
  ganttMetaDataURLObject.gantt.dialog_parent_linkfunction = getOrderRoute;
  calendarMetaDataURLObject.calendar.dialog_linkfunction = getOrderDetailsRoute;

  return (
    <LocationContext.Provider value={{
      prevLocation : loc,
      updatePrevLocation : updatePrevLocation
  
  }}>
    <div className="pageWidth">
      <Switch>
        {appRoutes.default &&
          appRoutes.relative.map((app, index) => {
            return (
              <Route
                exact={app.exact}
                path={app.path}
                key={index}
                render={(props) =>
                  React.createElement(
                    PageRouteComponents[app.component]
                      .id,
                    {
                      ...props,
                      ...PageRouteComponents[
                        app.component
                      ].props,
                    }
                  )
                }
              />
            );
          })}
      </Switch>
      {/* Dialog rendering component */}
      {tableMetaDataURLObject.table.htmlEnable && x && y && (
        <div id="popupDialog" style={{ position: "absolute", left: x, top: y }}>
          <TestComponent
            open={open}
            onClose={handleClickClose}
            x={x}
            y={y}
          ></TestComponent>
        </div>
      )}

      <podiumContexts.PageContext.Provider
         value={{ columns,  updateColumns, updateTableData }} >
      
      <div className="topTitleSearchWrap">
      <CustomiseWrapperComponent
        id={id}
        name={"table"}
        customiseMetaData={tableMetaDataURLObject?.customise}
        saveUrl={customiseSaveURI}
        appURI={customiseAppURI}
        defineOrderViewUrl={customiseDefineOrderViewURI}
        getCustomViewRoute={getCustomViewRoute}
        handleCustomiseUpdate= {handleCustomiseUpdate}
        parentDefineOrderURI = {parentDefineOrderURI}
        customiseDefineOrderLineItemsURI = {customiseDefineOrderLineItemsURI}
        open={openCustomized}
        setOpen={setOpenCustomized}
        customHeaders={customHeaders}
        setCustomHeaders={setCustomHeaders}
        customLineHeaders={customLineHeaders}
        setCustomLineHeaders={setCustomLineHeaders}
        setDoubleView = {setDoubleView}
      />
      <FilterWrapperComponent
        id={id}
        metadata={tableMetaDataURLObject?.filters}
        dataURI={tableDataURL}
        defineApiURI={customiseDefineOrderViewURI}
        URL={URL}
        externalReferenceURI={externalReferenceURI}
        changeCurrentTab={changeCurrentTab}
        currentTab={currentTab}
        tableMeta={tableMetaDataURLObject.table}
      />
      </div>
      
      <div>
      <div className="filterToolbar">
        <div className="tableViews">

          <FormControl variant="outlined" className={classes.formControlSelect}>
            <Select
              native
              value="Group by"
              className={classes.groupByField}
            >
              <option value="Group by">Group by</option>
            </Select>
          </FormControl>

          {currentTab === 0 && 
          <div className="toggleViews">
            <button id="btnHalfView" className={`btnHalfView ${doubleView ? 'activeBtn' : ''}`}  onClick={()=> setDoubleView(true)}>
              <span></span>
            </button>
            <button className={`btnFullView ${doubleView ? '' : 'activeBtn'}`} onClick={()=> setDoubleView(false)}>
              <span></span>
            </button>
          </div>
          }
        </div>

        <div className="actionTabs">
          <div className="actionButtons">
            <button className="btnEdit" onClick={handleCustomisedClick} >
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3.99992 20.0003H7.99992L18.4999 9.5003C19.0304 8.96987 19.3283 8.25045 19.3283 7.5003C19.3283 6.75016 19.0304 6.03074 18.4999 5.5003C17.9695 4.96987 17.2501 4.67188 16.4999 4.67188C15.7498 4.67187 15.0304 4.96987 14.4999 5.5003L3.99992 16.0003V20.0003Z" stroke="#323232" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M13.5 6.5L17.5 10.5" stroke="#323232" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
            </button>

            <button
              title="print"
              icon="print"
              showBtn={true}
              showIcon={true}
              parentClassName="printButtonWrapper"
              className="printButton btnPrint"
              onClick={() => {loadPrint()}}
            >
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M17 17H19C19.5304 17 20.0391 16.7893 20.4142 16.4142C20.7893 16.0391 21 15.5304 21 15V11C21 10.4696 20.7893 9.96086 20.4142 9.58579C20.0391 9.21071 19.5304 9 19 9H5C4.46957 9 3.96086 9.21071 3.58579 9.58579C3.21071 9.96086 3 10.4696 3 11V15C3 15.5304 3.21071 16.0391 3.58579 16.4142C3.96086 16.7893 4.46957 17 5 17H7" stroke="#323232" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M17 9V5C17 4.46957 16.7893 3.96086 16.4142 3.58579C16.0391 3.21071 15.5304 3 15 3H9C8.46957 3 7.96086 3.21071 7.58579 3.58579C7.21071 3.96086 7 4.46957 7 5V9" stroke="#323232" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                <path d="M15 13H9C7.89543 13 7 13.8954 7 15V19C7 20.1046 7.89543 21 9 21H15C16.1046 21 17 20.1046 17 19V15C17 13.8954 16.1046 13 15 13Z" stroke="#323232" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
              </svg>
            </button>
          </div>
          <div className="toggleTabsFilter">
            <ToggleButtonComponent id={id} changeCurrentTab={changeCurrentTab} />
          </div>
        </div>

      </div>

        <TabPanel value={currentTab} index={0}>
          {
            <PrintableWrapperComponent
              {...printableMetaDataURLObject.table}
            >
               <TableViewPage
                            id={id}
                            ganttMetaDataURLObject={tableMetaDataURLObject}
                            formObjectMetaDataURLObject={formObjectMetaDataURLObject}
                            tableDataURL={tableDataURL}
                            orderDetailsDataURL={orderDetailsDataURL}
                            customiseDefineOrderViewURI={
                                customiseDefineOrderViewURI
                            }
                            customiseDefineOrderLineItemsURI={customiseDefineOrderLineItemsURI}
                            parentDefineOrderURI={parentDefineOrderURI}
                            method={"post"}
                            URL={URL}
                            tableMetaDataURLObject={tableMetaDataURLObject}
                            treeViewCollection={treeViewCollection}
                            caseRequestURLs={caseRequestURLs}
                            doubleView={doubleView}
               />
            </PrintableWrapperComponent>
          }
        </TabPanel>

        <TabPanel value={currentTab} index={1}>
          <GanttWrapperCompenent
            id={id}
            metadata={ganttMetaDataURLObject?.gantt}
            dataURI={ganttDataURL}
            detailsDataURI={orderDetailsDataURL}
            URL={""}
            formObjectMetaDataURL={formObjectMetaDataURLObject}
            tableMetadata={tableMetaDataURLObject}
            actionBarURI={URL}
            defineURI={customiseDefineOrderLineItemsURI}
            customiseDefineOrderViewURI={customiseDefineOrderViewURI}
            parentDefineOrderURI={parentDefineOrderURI}
            tableDataURI={tableDataURL}
            getParentData={getParentQuantity}
            modifyColumns={modifyColumns}
            ganttScale={scales}
            caseRequestURLs={caseRequestURLs}
          />
        </TabPanel>

        <TabPanel value={currentTab} index={2}>
          <PrintableWrapperComponent
                {...printableMetaDataURLObject.calender}
              >
            <SchedulerWrapperComponent
              id={id}
              metaData={calendarMetaDataURLObject.calendar}
              formObjectMetaDataURL={formObjectMetaDataURLObject}
              dataURI={calendarDataURL}
              detailsDataURL={orderDetailsDataURL}
              URL={""}
              tableMetadata={tableMetaDataURLObject}
              actionBarURI={URL}
              parentDefineOrderURI={parentDefineOrderURI}
              detailsDataURI={orderDetailsDataURL}
              defineURI={customiseDefineOrderLineItemsURI}
              customiseDefineOrderViewURI={
                customiseDefineOrderViewURI
              }
              tableDataURI={tableDataURL}
              caseRequestURLs={caseRequestURLs}
            />
          </PrintableWrapperComponent>
        </TabPanel>
        {/* {orderId & lineItemId ? <OrderDetails></OrderDetails> : null} */}
      </div>
      </podiumContexts.PageContext.Provider>
    </div>
   </LocationContext.Provider>
  );
};

export default OrderStatusTracking;
