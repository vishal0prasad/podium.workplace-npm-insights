import React from 'react';

import Grid from '@material-ui/core/Grid';
import Order from '../order';
import {getParentQuantity} from './functions';
import GanttMetadata from '../../../../data/ganttdata/ganttmetadata.json';
import { apiUrls } from '../../../../configs/apiUrls';

const GanttPageMetadata = {
    pageElement: {
        componentList: [
                     {
                id: "1",
                componentName: "Gantt",
                componentWidth: 12,
                /* Component: GanttWrapper, */
                componentData: {
                    MetaData: GanttMetadata,
                    DataURI: apiUrls.order.ganttView.url,
                    getParentData:getParentQuantity,
                    URL: {},
                    Arg: {}
                },
            }
        ]
    }
};

const GanttPage = () => {
    return (
        <div>
            <Grid container spacing={3}>
                {GanttPageMetadata.pageElement.componentList.map((item, index) => (
                    <Order {...item} key={index} />
                ))}
            </Grid>
        </div>
    )

}

export default GanttPage;