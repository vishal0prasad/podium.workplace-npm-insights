import { isEqual } from "date-fns";
import { UNIT_OF_MEASUREMENT, gsDayNames } from './constants';

export const getQuantityWithKgs = (data) => {
  let flag = 0;
  for (let i = 0; i < data.length; i++) {
    if (data[i].ord_ln_item_uom !== UNIT_OF_MEASUREMENT) {
      flag = 1;
      break;
    }
  }
  console.log("getQuantityWithKgs", flag == 0 ? data : [], data);
  return flag == 0 ? data : [];
}

export const modifyColumns = (extData: any, parent_open:boolean) => {
  for (let d of extData.data) {
    d.end_date += " 23:59:00";
    d.start_date += " 00:00:00";
    if (d.type === "Project") {
      const tasksGroupById = extData.data.filter((row: { parent: any }) => row.parent === d.id);
      const tasksQuantitySum = getQuantityWithKgs(tasksGroupById).map((d: { qty: any; }) => d.qty)
        .reduce((a: any, b: any) => parseInt(a) + parseInt(b), 0)
      d.qty = tasksQuantitySum ? tasksQuantitySum : 0;
      d.open = parent_open
    }
  }
  return extData
}

export const getParentQuantity = (gantt, task) => {
  let qty = 0;
  let flag = 1;
  gantt.eachTask(
    function (ctask) {
      qty += ctask.qty;
      if (ctask.ord_ln_item_uom !== UNIT_OF_MEASUREMENT) {
        flag = 0;
      }
    },
    [task.id]
  );
  if (flag == 0) {
    return "";
  }
  return qty + " " + UNIT_OF_MEASUREMENT;
};
const checkDateEquality = (date: any) => {
  const today = new Date();
  today.setHours(0);
  today.setMinutes(0);
  today.setSeconds(0);
  today.setMilliseconds(0);
  if (
    isEqual(
      new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        0,
        0,
        0,
        0
      ),
      today
    )
  ) {
    return true;
  }

  return false;
};

const monthScaleTemplate = function (date: Date) {
  let day = gsDayNames[date.getDay()][0];
  let cdate = date.getDate();
  if (!checkDateEquality(date)) {
    return `<p style="font-size:12px;line-height:1.5;margin:0px; justify-content: center; align-items: center; display: flex; flex-wrap: wrap; flex-direction: column;">${day}<br/><span style="height: 24px; width: 24px; display:flex; justify-content: center; align-items: center;">${cdate}</span> </p>`;
  }
  if (date.getDay() == 0) {
    return `<p style="font-size:12px;line-height:1.5;margin:0px;border:2px solid black">${day}<br/><span style="color:#ffffff; display:flex; background: #19838c;border-radius: 50%; height: 24px; width: 24px; justify-content: center; align-items: center;">${cdate}</span></p>`
  }
  return `<p style="font-size:12px;line-height:1.5;margin:0px;">${day}<br/><span style="color:#ffffff; display:flex; background: #19838c;border-radius: 50%; height: 24px; width: 24px; justify-content: center; align-items: center;">${cdate}</span></p>`;
};

export const scales = [
  {
    unit: "month",
    step: 1,
    format: "<strong>" + "%M" + " " + "%Y" + "</strong>",
  },
  {
    unit: "day", step: 1, format: monthScaleTemplate, css: function (date: Date) {
      if (date.getDay() === 0) {
        return "weekend-styles";
      }
    }
  }
];
