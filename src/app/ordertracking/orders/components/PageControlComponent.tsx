import React, { FC,useEffect } from 'react';
import { useParams } from "react-router-dom"

import { loadView } from "../functions"


const PageControlComponent: FC = (props:any) => {

    const {viewId, tabName, orderId, lineItemId} = useParams();
    useEffect(() => {
      loadView(viewId,props.id,orderId, lineItemId)
      // if(tabName=="gantt")
      // {
      //   props.changeCurrentTab(1)
      // }

    },[viewId])

    return null
}

export default PageControlComponent