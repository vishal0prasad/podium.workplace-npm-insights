//@ts-nocheck
import React,  { FC }  from 'react';
import { useParams } from "react-router-dom";

import { DrawerPanelWrapperComponent, FormObjectWrapperComponent } from '@udp-dataplatform/podiumuiengine'
import { ApplicationConfig } from "../../../../configs/application";
import { apiUrls } from '../../../../configs/apiUrls';
import {  getOrderDetailsRoute, getOrderRoute } from "../../../functions";

import ObjectFormMetaData from "../../../../data/formObjectMetaData.json";
import ActionsMetaData from "../../../../data/actionsMetaData.json";
import Mustache from "mustache";
import { Constants } from '../../constants';
import { getCutsomViewReuest } from '../../ordersAPI';


const caseRequestURLs = {
    getAll: ApplicationConfig.getUrl(apiUrls.caseRequest.getAllRequests.url, true),
    getEnquiryTypes: ApplicationConfig.getUrl(apiUrls.caseRequest.getEnquiryTypes.url, true),
    getForm: ApplicationConfig.getUrl(apiUrls.caseRequest.getRequestForm.url, true),
    submit: ApplicationConfig.getUrl(apiUrls.caseRequest.submitForm.url, true),
    getDetails: ApplicationConfig.getUrl(apiUrls.caseRequest.getRequestDetails.url, true)
  }

  const URL = {
    tenancyPrefix: ApplicationConfig.getUrl("", true),
    nonTenancyPrefix: ApplicationConfig.getUrl("", false),
  };

  const customiseDefineOrderLineItemsURI = ApplicationConfig.getUrl(apiUrls.order.defineViewLineItems.url,
    false
  );


let formObjectProps: any = {
    id: `${Constants.APP_CODE}_${Constants.PAGE_NAME}_OrderDetail_FormObject`,
    formObjectMetaDataURL : ObjectFormMetaData,
    metadata :  ActionsMetaData ,
    prefixURL : URL,
    parentDefineOrderURI :   ApplicationConfig.getUrl(apiUrls.order.defineParentView.url,false),
    DefineAPIAURI : customiseDefineOrderLineItemsURI,
    caseRequestURLs : caseRequestURLs,
    whichItemId: 0,
    whichLineItemId: 0,
    showItemDetails: true,
    parentURL : ApplicationConfig.getUrl(apiUrls.order.view.url,
      false
    )
}

let drawerPanelProps: any = {
        id: `${Constants.APP_CODE}_${Constants.PAGE_NAME}_OrderDetail_DrawerPanel`,
        // metadata: formObjectProps,
        // component : FormObjectWrapperComponent 
        formObjectMetaDataURL : ObjectFormMetaData,
        metadata :  ActionsMetaData ,
        prefixURL : URL,
        parentDefineOrderURI :   ApplicationConfig.getUrl(apiUrls.order.defineParentView.url,false),
        DefineAPIAURI : customiseDefineOrderLineItemsURI,
        caseRequestURLs : caseRequestURLs,
        whichItemId: 0,
        whichLineItemId: 0,
        showItemDetails: true,
        parentURL : ApplicationConfig.getUrl(apiUrls.order.view.url,
          false
        )
}


const OrderDetails : FC = () => {
  const {orderId, lineItemId} = useParams();
  // have to refactor with form object
  drawerPanelProps.formObjectMetaDataURL.formObject.layouts[0].view.parentView.sections[1].metaData.table.dialog_linkfunction = getOrderDetailsRoute;
  drawerPanelProps.formObjectMetaDataURL.formObject.layouts[0].view.parentView.routeToParent = getOrderRoute;
  drawerPanelProps.whichItemId=orderId
  drawerPanelProps.whichLineItemId=lineItemId
  let dataURL = ApplicationConfig.getUrl(apiUrls.order.list.url,false)  + Mustache.render(lineItemId?drawerPanelProps.formObjectMetaDataURL.formObject.url:
     drawerPanelProps.formObjectMetaDataURL.formObject.parentUrl, { ord_id: orderId, ord_ln_item_id: lineItemId });
  drawerPanelProps.dataURL=dataURL
  return (
        <div>
            <DrawerPanelWrapperComponent {...drawerPanelProps}></DrawerPanelWrapperComponent> 
        </div>
    )

}


export default OrderDetails;