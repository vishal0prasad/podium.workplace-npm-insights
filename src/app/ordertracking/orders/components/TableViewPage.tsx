import React, { useState, useEffect, useContext } from 'react';
import { TableWrapperComponent } from "@udp-dataplatform/podiumuiengine";
import { store, actions } from "@udp-dataplatform/podiumuiengine";
import {tableStyles} from "../styles/tableStyles";
import orderLineStatusTracking from "../../../data/orderLineStatusTracking.json";
import { LocationContext } from '@udp-dataplatform/podiumuiengine'
import { useHistory}  from "react-router-dom";


export const newFunc = ()=>{
};

const TableViewPage = (props: any) => {
  const classes = tableStyles();

  const { id,
    formObjectMetaDataURLObject,
    tableDataURL,
    orderDetailsDataURL,
    customiseDefineOrderViewURI,
    customiseDefineOrderLineItemsURI,
    URL,
    parentDefineOrderURI,
    tableMetaDataURLObject,
    treeViewCollection,
    caseRequestURLs,
    doubleView 
  } = props;

  const [previousSelectedRow ,setPreviousSelectedRow] = useState('');
  const ctxLocation : any = useContext(LocationContext);
  let history = useHistory();

  
  const onOrderClick = (whichOrder: number, cb:()=>string) => {
    if(cb){
      let rowID= cb();
      setPreviousSelectedRow(rowID);
    }
    const requestConfigs = store.getState().TrackTrace_Orders_OrderStatus_child_table.requestData;
    store.dispatch(actions.TableActions.updateRequestData({
      ...requestConfigs,
      query: `ord_num in ('${whichOrder}')`,
      refreshFlag: true
    }, `${id}_child`));
  }

  tableMetaDataURLObject.table.template = (text: string, row: any, col: any) => {
    const [link_type, link_name] = tableMetaDataURLObject.table.link_by?.split(":") || [""];
      // functionName  = newFunc ;
    if (col.id === link_name) {
        return `<span class='notClickable'>
        <input type='checkbox' value={${row.ord_num}} />
        <span class='clickableLink'>${text}</span>
        </span>`
    } 
    else if (col.id === 'ord_status') {
      let html = '';
      if (tableMetaDataURLObject.table.statusCode) {
        tableMetaDataURLObject.table.statusCode.map((key:any, index:any)=> {
          if (key===text) {
            html = `<span class='statusChip ${key}'>${text}</span>`;
          } 
        })
      }
      return html; 
    }
     else {
        return text;
    }
}

orderLineStatusTracking.table.template = (text: string, row: any, col: any) => {
  const [link_type, link_name] = tableMetaDataURLObject.table.link_by?.split(":") || [""];
    // functionName  = newFunc ;
  if (col.id === 'ord_ln_item_id') {
      return `<span class='clickableLink'>${text}</span>`
  }
  else if (col.id === 'ord_status') {
    let html = '';
    if (tableMetaDataURLObject.table.statusCode) {
      tableMetaDataURLObject.table.statusCode.map((key:any, index:any)=> {
        if (key===text) {
          html = `<span class='statusChip ${key}'>${text}</span>`;
        } 
      })
    }
    return html; 
  }
   else {
      return text;
  }
}
tableMetaDataURLObject.table.cellClick = (row,col, e, treegrid) => {
  const [link_type, link_name] = tableMetaDataURLObject.table.link_by?.split(":") || [""]  ;
  
  if (e.target.className === 'clickableLink') {
      // dispatch action to open the drawer pannel
      /* store.dispatch(actions.TableActions.getOrderDetails({
        showItemDetails: true,
        whichItemId: row.ord_id,
        whichLineItemId: row.ord_ln_item_id,
        whichItemNumber: row.ord_num
      },`${id}`)); */
      ctxLocation.updatePrevLocation(window.location.pathname)
      history.push(tableMetaDataURLObject.table.dialog_parent_linkfunction(row.ord_id, row.ord_ln_item_id))
    }
    else{
      console.log("previousSelectedRow" , previousSelectedRow);
      previousSelectedRow? treegrid.removeRowCss(previousSelectedRow, "myCustomClass"): "";  
      treegrid.addRowCss(row.id, "myCustomClass");
      onOrderClick(row.ord_num);
    }
    setPreviousSelectedRow(row.id);
  }

  return (

    <div className={classes.splitTableView}>
      <div className={`${classes.tableLeft} ${doubleView ? '' : classes.tableInFull }`}>
        <TableWrapperComponent
          id={`${id}`}
          metaDataURL={tableMetaDataURLObject}
          formObjectMetaDataURL={formObjectMetaDataURLObject}
          dataURL={tableDataURL}
          detailsDataURL={orderDetailsDataURL}
          customiseDefineOrderViewURI={
            customiseDefineOrderViewURI
          }
          DefineAPIAURI={customiseDefineOrderLineItemsURI}
          parentDefineOrderURI={parentDefineOrderURI}
          method={"post"}
          actionBarURL={URL}
          groupKey={tableMetaDataURLObject?.table?.groupKey}
          treeViewCollection={treeViewCollection}
          caseRequestURLs={caseRequestURLs}
          onOrderClick={onOrderClick}
        />
      </div>

       <div className={`${classes.tableRight} ${doubleView ? '' : classes.tableRightCollapse}`}>
         <TableWrapperComponent
          id={`${id}_child`}
          metaDataURL={orderLineStatusTracking}
          formObjectMetaDataURL={formObjectMetaDataURLObject}
          dataURL={tableDataURL}
          detailsDataURL={orderDetailsDataURL}
          customiseDefineOrderViewURI={
            customiseDefineOrderViewURI
          }
          DefineAPIAURI={customiseDefineOrderLineItemsURI}
          parentDefineOrderURI={parentDefineOrderURI}
          method={"post"}
          actionBarURL={URL}
          groupKey={tableMetaDataURLObject?.table?.groupKey}
          treeViewCollection={treeViewCollection}
          caseRequestURLs={caseRequestURLs}
        />
      </div>
    </div>
  )

}

export default React.memo(TableViewPage);