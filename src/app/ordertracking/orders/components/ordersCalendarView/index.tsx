import React from 'react';

import Grid from '@material-ui/core/Grid';

import Order from '../order';
import CalendarMetadata from '../../../../data/schedulerdata/metadata.json';
import { apiUrls } from '../../../../configs/apiUrls'

const CalendarPageMetadata = {
    pageElement: {
        componentList: [
            {
                id: "1",
                componentName: "Calendar",
                componentWidth: 12,
                /* Component: SchedulerWrapper, */
                componentData: {
                    MetaData: CalendarMetadata,
                    DataURI: apiUrls.order.calendarView.url,
                    URL: {},
                    Arg: {}
                },
            }
        ]
    }
};

const CalendarPage = () => {
    return (
        <div>
            <Grid container spacing={3}>
                {CalendarPageMetadata.pageElement.componentList.map((item, index) => (
                    <Order {...item} key={index} />
                ))}
            </Grid>
        </div>
    )

}

export default CalendarPage;