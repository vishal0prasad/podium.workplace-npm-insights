import { store, actions } from "@udp-dataplatform/podiumuiengine";

import { apiUrls } from "../../configs/apiUrls";
import { ApplicationConfig } from "../../configs/application";
import { CustoViewConstants } from "./constants";
import { getCutsomViewReuest, getCutsomDetailViewReuest } from "./ordersAPI";
import OrdersStatusTrackingMetaData from "../../data/ordersStatusTracking.json";

export const loadView = (id: string, stateInstanceId: string, orderId: string, lineItemId: string) => {
  let viewName="";
  
  let viewDescription="";
  // if(orderId && lineItemId)
  // {
  //   store.dispatch(actions.TableActions.getOrderDetails({
  //     showItemDetails: true,
  //     whichItemId: orderId,
  //     whichLineItemId: lineItemId,
  //   }, stateInstanceId));
  // }
  
  if (id) {
   
    getCutsomViewReuest(
      ApplicationConfig.getUrl(apiUrls.userAppPrefernece.pageView.url, true),
      id
    ).then((res: any) => {
      console.log(res.data);
      var obj = JSON.parse(res.data[0].userConfig);
      viewName=res.data[0].viewName;
      viewDescription=res.data[0].viewDescription;
      console.log('tese',id,viewName,viewDescription);
      actions.setCustomControlSheet({
        id,
        viewName,
        viewDescription 
      });
      let checkObject = {
        column: false,
        query: false,
        defineColumnsObject: false,
      };

      Object.keys(obj).map((key: string) => {
        if (
          key === CustoViewConstants.USER_CONFIG_COLUMN_KEY ||
          key === CustoViewConstants.USER_CONFIG_QUERY_KEY ||
          key === CustoViewConstants.USER_CONFIG_DEFINE_COLUMN_KEY
        )
          checkObject[key] = true;
      });
      if (
        checkObject.column &&
        checkObject.query &&
        checkObject.defineColumnsObject
      ) {
        const arr = Object.keys(obj?.column).map((key) => key);
        // props.handleToggle();
        store.dispatch(
          actions.TableActions.setCustomColumns(arr, stateInstanceId)
        );
        actions.setCustomiseColumns(arr)
        store.dispatch(
          actions.TableActions.setCustomQuery(obj.query, stateInstanceId)
        );
        store.dispatch(
          actions.TableActions.setCustomJsonQuery(obj.jsonQuery, stateInstanceId)
        );
        const requestConfigs={
          columns: arr,
          query: obj.query,
          jsonQuery: obj.jsonQuery,
          sort: "",
          refreshFlag: true
        } as any
        if(OrdersStatusTrackingMetaData.table.infiniteScroll){
          requestConfigs.pageNo=1
        }
        store.dispatch(
          actions.TableActions.updateRequestData(
            requestConfigs,
            stateInstanceId
          )
        );
        if(obj.defineColumnsObject)
        {
        actions.setDefineColumnsObject(obj.defineColumnsObject);
        }
        actions.setFilterBadge(true);
        actions.setSave(false);
        actions.refreshCustomise(true);
      } 

      
      store.dispatch(
        actions.TableActions.updateTableTitle(viewName, stateInstanceId)
      );

      

      store.dispatch(
        actions.TableActions.updateTableDescription(viewDescription, stateInstanceId)
        
      );
       actions.setSave(false);
    });


    // For child table
    getCutsomDetailViewReuest( 
      ApplicationConfig.getUrl(apiUrls.userAppPrefernece.pageView.url, true),
      id
    ).then((res: any) => {
      var obj = JSON.parse(res.data[0].userConfig);
      var child_id = `${stateInstanceId}_child`
      viewName=res.data[0].viewName;
      let checkObject = {
        column: false,
        query: false,
        defineColumnsObject: false,
      };

      Object.keys(obj).map((key: string) => {
        if (
          key === CustoViewConstants.USER_CONFIG_COLUMN_KEY ||
          key === CustoViewConstants.USER_CONFIG_QUERY_KEY ||
          key === CustoViewConstants.USER_CONFIG_DEFINE_COLUMN_KEY
        )
          checkObject[key] = true;
      });
      if (
        checkObject.column &&
        checkObject.query &&
        checkObject.defineColumnsObject
      ) {
        const arr = Object.keys(obj?.column).map((key) => key);
        // props.handleToggle();
        store.dispatch(
          actions.TableActions.setCustomColumns(arr, child_id)
        );
        actions.setCustomiseLineColumns(arr)
        store.dispatch(
          actions.TableActions.setCustomQuery(obj.query, child_id)
        );
        store.dispatch(
          actions.TableActions.setCustomJsonQuery(obj.jsonQuery, child_id)
        );
        const requestConfigs={
          columns: arr,
          query: obj.query,
          jsonQuery: obj.jsonQuery,
          sort: "",
          refreshFlag: true
        } as any
        if(OrdersStatusTrackingMetaData.table.infiniteScroll){
          requestConfigs.pageNo=1
        }
        store.dispatch(
          actions.TableActions.updateRequestData(
            requestConfigs,
            child_id
          )
        );
        if(obj.defineColumnsObject)
        {
        actions.setDefineColumnsObject(obj.defineColumnsObject);
        }
        actions.setFilterBadge(true);
        actions.setSave(false);
        actions.refreshCustomise(true);
      } 
      store.dispatch(
        actions.TableActions.updateTableTitle(viewName, child_id)
      );
       actions.setSave(false);
    });
  }
  
};
