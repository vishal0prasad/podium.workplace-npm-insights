
import { makeStyles } from '@material-ui/styles';

export const tableStyles = makeStyles({
  splitTableView: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  tableLeft: {
    width:"55%",
    background: "transparent",
    transition: "all 0.75s ease-in",
    // boxShadow:"3px 0px 2px rgb(0 0 0 / 25%)",
    // zIndex:1,
  },
  tableInFull: {
    width:"100%",
    background: "transparent",
    transition: "all 0.75s ease-out",
  },
  tableRight: {
    width:"45%",    
    background: "transparent",
    transition: "all 0.75s ease-in",
    "& .loadResultsContainer": {
      padding:0,
    },
    "& .dhx_data-wrap" :{
      background: "#EAFDFD",
      boxShadow: "inset 0px -1px 0px #DDE3E7",
    },
    "& .dhx_grid-cell:first-child": {
      boxShadow: "inset 62px 0px 18px -59px #dde3e7",
    }
  },
  tableRightCollapse: {
    width:"0%",
    opacity: "0",
    transition: "all 0.75s ease-out",
  },
  formControlSelect: {
    position:"relative",
    "&:after": {
      content: "''",
      height: "8px",
      width: "8px",
      background: "transparent",
      borderLeft: "2px solid #9BA4AB",
      borderBottom: "2px solid #9BA4AB",
      transform: "rotate(-45deg)",
      top: "12px",
      right: "8px",
      position: "absolute",
    },
    "& .MuiOutlinedInput-root": {
      "& :hover" :{
        borderColor:"#DCDCDC"
      }
    },
    "& select": {
      padding: "0",
      height: "40px",
      width: "117px",
      background: "#FFFFFF",
      border: "2px solid #DCDCDC",
      boxSizing: "border-box",
      borderRadius: "3px",
      textAlign: "center",
      fontSize: "16px",
      color:"#4F4F4F",
      "& .MuiOutlinedInput-notchedOutline" : {
        borderColor:"transparent",
        padding:0,
      },      
    },
    "& .MuiSelect-outlined.MuiSelect-outlined": {
      paddingRight:0,
      paddingLeft:"4px",
    },
    "& .MuiSelect-icon": {
      display:"none",
    }
  }
})
