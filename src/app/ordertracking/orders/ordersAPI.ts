import Mustache from "mustache";

import { HttpService } from "@udp-dataplatform/podiumuiengine";

import { apiUrls }  from "../../configs/apiUrls";

const httpService = new HttpService();

export const getCutsomViewReuest = (pageViewUri: string, id: string) => {

    const custoViewURI = Mustache.render(apiUrls.order.customView.url,{viewId:id})
    return httpService.get(
      `${pageViewUri}${custoViewURI}`
    );
  };

  export const getCutsomDetailViewReuest = (pageViewUri: string, id: string) => {

    const custoViewURI = Mustache.render(apiUrls.order.customDetailView.url,{viewId:id})
    return httpService.get(
      `${pageViewUri}${custoViewURI}`
    );
  };