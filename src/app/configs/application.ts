const base = window as any;
export class ApplicationConfig {
    public static domainURL: any = process.env.REACT_APP_API_ENDPOINT;
    //public static domainURL = base.env.API_ENDPOINT_URI;

    public static routerURL: any = process.env.REACT_APP_ORDER_TRACKING_ROUTER;
    //public static routerURL = base.env.ORDER_TRACKING_ROUTER_ENDPOINT_URI;

    public static routerTenancyURL: any = process.env.REACT_APP_ORDER_TRACKING_TENANCY_ROUTER;
    //public static routerTenancyURL: any = base.env.ORDER_TRACKING_TENANCY_ROUTER_ENDPOINT_URI;

    public static staticAPIText = 'api/';

    public static getUrl(url: string, useRouterTenancyURL: boolean): string {
        if (useRouterTenancyURL) {
            return (ApplicationConfig.domainURL + ApplicationConfig.staticAPIText + ApplicationConfig.routerTenancyURL + url);
        } else {
            return (ApplicationConfig.domainURL + ApplicationConfig.staticAPIText + ApplicationConfig.routerURL + url);
        }
    }

}

