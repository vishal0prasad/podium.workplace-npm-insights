import urls from "./data/apiUrls.json";

export interface UrlItem {
    name: string;
    url: string;
}

export interface Experience {
    data: UrlItem;
    menu: UrlItem;
}

export interface Order {
  customDetailView: UrlItem;
  defineParentView: any;
  defineView: UrlItem;
  defineViewLineItems: UrlItem;
  list: UrlItem;
  view: UrlItem;
  customView: UrlItem;
  historyView: UrlItem;
  ganttView: UrlItem;
  calendarView: UrlItem;
  filter: UrlItem;
}
export interface UserAppPrefernece {
    appCode: UrlItem;
    masterControlSheets: UrlItem;
    customControlSheets: UrlItem;
    saveComponent: UrlItem;
    pageView: UrlItem;
    deleteView: UrlItem;
}
export interface Product {
    view: UrlItem;
}
export interface uploadCSV {
    uploadData: UrlItem;
    getSupplierOption: UrlItem;
    getProjectsOption: UrlItem;
    getcategoriesOption: UrlItem;
}

export interface CaseRequest {
  getAllRequests: UrlItem;
  getEnquiryTypes: UrlItem;
  getRequestForm: UrlItem;
  submitForm: UrlItem;
  getRequestDetails: UrlItem;
}

export interface Notification {
  reminder: UrlItem;
}

export interface ApiUrls {
  product: Product;
  experience: Experience;
  order: Order;
  userAppPrefernece: UserAppPrefernece;
  uploadCSV: uploadCSV;
  caseRequest: CaseRequest;
  notification: Notification;
}

export const apiUrls: ApiUrls = { ...urls };
