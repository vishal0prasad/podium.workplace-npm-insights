import paths from "./data/appLinks.json";

export interface Orders {
  customView: string;
  order : string;
  orderDetails: string;
}

export interface appLinks {
  orders: Orders;
}

export const appLinks: appLinks = { ...paths };
