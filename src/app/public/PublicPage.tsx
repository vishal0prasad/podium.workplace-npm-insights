import React, { FC } from 'react';
import { Switch,Route } from "react-router-dom"

import TestPage  from "./TestPage"


import appRoutes from "../configs/data/appRoutes"
import { PublicPageWrapperComponent } from '../..';

const publicRoutePages: any = {
    testPage: {
      id: TestPage,
      props : { }
    }
   };    

const PublicPage: FC = (props:any) => {

    return (<div>
            <Switch>
               {appRoutes.default &&
                    appRoutes.publicRelative.map((app, index) => {
                    return (
                        <Route
                        exact= {app.exact}
                        path={app.path}
                        key={index}
                        render={(props) =>
                            React.createElement(
                                publicRoutePages[app.component].id 
                            ,
                            {...props, ... publicRoutePages[app.component].props }
                            )
                        }
                        />
                    );
                    })}
        </Switch>
    </div>)

}

export default PublicPage