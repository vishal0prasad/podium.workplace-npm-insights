import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
  en: {
    translation: {

      // Order details screen
      
        "id":  "ID",
        "supplierSKU":  "Product SKU",
        "quantity" :  "Quantity",
        "orderNumber": "Order Number",
        "orderBy": "Ordered By",
        "ideleted": "Is deleted",
        "vendor": "Vendor",
        "orderDescription": "Description",
        "name": "Name",
        "sched": "Schedule Number",
        "bar": "Bar (Tonnes)",
        "sch": "Scheduled Location",
        "city": "City",
        "load": "Load Date",
        "mesh": "Mesh (tonnes)",
        "type": "Order Type",
        "other": "Other",
        "total": "Total",
        "markpl": "Markpl",
        "onsite": "Planned Site Date",
        "address": "Delivery Address",
        "custref": "Customer Reference",
        "taxCust": "Tax Customer",
        "activate": "Production Ready Date",
        "costcode": "Cost Code",
        "customer": "Customer",
        "despatch": "Despatch Date",
        "scheduler": "Scheduler Name",
        "plannedDeliveryDate": "Planned Delivery Date",
        
        // table view headers

        "ord_id" : "Order ID",
        "ord_by" : "Order By",
        "ord_num": "Order Number",
        "ord_ln_item_id" : "Order LineItem ID",
        "suppliersku" : "Product SKU",
        "ord_plan_deliv_dt" : "Planned Delivery Date",
        "order_delivery_date" : "Planned Delivery Date",
        "crt_dt" : "Creation date",
        "ord_status" : "Order Status",
        "crt_by" : "Created By",
        "lst_mod_dt" : "Last Modified Date",
        "lst_mod_by" : "Last Modified By",
        "qty" : "Quantity",
        "ord_des" : "Order Description",

        // Your Control Sheets section
        "viewName" : "Control Sheet",
        "viewDescription" : "Description",

        //table error message
        "tableErrorMessage":"No Data for the selected filter options"
    }
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en",

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safe from xss
    }
  });

  export default i18n;